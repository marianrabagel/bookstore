﻿using BookStore.Domain.Entities;

namespace BookStore.REST.Repository.Contracts.Mappers
{
    public interface IProductTypeMapper
    {
        DataAccess.Entities.ProductType FillDataAccessProductTypeFromDomain(Domain.Entities.ProductType productType);
        ProductType FillDomainProductTypeFromDataAccess(DataAccess.Entities.ProductType productType);
    }
}