﻿
using BookStore.Domain.Entities;

namespace BookStore.REST.Repository.Contracts.Mappers
{
    public interface IProductMapper
    {
        DataAccess.Entities.Product FillDataAccessProductFromDomain(Domain.Entities.Product product);
        Product FillDomainProductFromDataAccess(DataAccess.Entities.Product product);
    }
}
