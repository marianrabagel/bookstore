﻿using System.Collections.Generic;
using BookStore.Domain.Entities;

namespace BookStore.REST.Repository.Contracts
{
    public interface IProductTypesRepository
    {
        void PersistProductTypes(IEnumerable<ProductType> productTypes);
        ProductTypesList GetProductTypes();
    }
}
