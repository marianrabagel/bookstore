﻿using BookStore.Domain.Entities;

namespace BookStore.REST.Repository.Contracts
{
    public interface IProductsRepository
    {
        void PersistProductsImportData(ProductsList productsList);
        ProductsList GetProducts();
    }
}
