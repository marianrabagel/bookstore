﻿using BookStore.REST.DataAccess.Contracts;

namespace BookStore.REST.Repository.Contracts
{
    public abstract class BaseRepository 
    {
        protected BaseRepository(IDataContext context)
        {
            DataContext = context;
        }

        public IDataContext DataContext { get; }
    }
}
