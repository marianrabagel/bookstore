﻿using System;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.Repository.Contracts;
using BookStore.REST.Repository.Contracts.Mappers;

namespace BookStore.REST.Repository
{
    public class ProductsRepository : BaseRepository, IProductsRepository
    {
        private readonly IProductTypesRepository productTypesRepository;
        private readonly AbstractListValidator<ProductsList, Product> productsListValidator;
        private readonly IProductMapper productMapper;

        public ProductsRepository(IDataContext context, IProductMapper productMapper, 
            IProductTypesRepository productTypesRepository, AbstractListValidator<ProductsList, Product> productsListValidator)
            : base(context)
        {
            this.productTypesRepository = productTypesRepository;
            this.productsListValidator = productsListValidator;
            this.productMapper = productMapper;
        }

        public ProductsList GetProducts()
        {
            var productsList = new ProductsList(productsListValidator);
            
            foreach (var product in DataContext.Products)
            {
                var domainProduct = productMapper.FillDomainProductFromDataAccess(product);
                productsList.Add(domainProduct);
            }
            
            return productsList;
        }

        public void PersistProductsImportData(ProductsList productsList)
        {
            if (productsList == null)
            {
                throw new ArgumentNullException(nameof(productsList));
            }
            if (productsList.Count == 0)
            {
                return;
            }
            
            foreach (var product in productsList)
            {
                DataAccess.Entities.Product dataAccessProduct = productMapper.FillDataAccessProductFromDomain(product);
                DataContext.Products.Add(dataAccessProduct);
            }

            DataContext.SaveChanges();
        }
    }
}
