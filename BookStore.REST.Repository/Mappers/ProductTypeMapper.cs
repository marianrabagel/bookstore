﻿using System;
using BookStore.Domain.Entities;
using BookStore.REST.Repository.Contracts.Mappers;
using FluentValidation;

namespace BookStore.REST.Repository.Mappers
{
    public class ProductTypeMapper : IProductTypeMapper
    {
        private readonly AbstractValidator<ProductType> productTypeValidator;

        public ProductTypeMapper(AbstractValidator<ProductType> productTypeValidator)
        {
            this.productTypeValidator = productTypeValidator;
        }

        public DataAccess.Entities.ProductType FillDataAccessProductTypeFromDomain(ProductType productType)
        {
            if (productType == null)
            {
                throw new ArgumentNullException(nameof(productType));
            }

            return new DataAccess.Entities.ProductType { Name = productType.Name.ToUpperInvariant() };
        }

        public ProductType FillDomainProductTypeFromDataAccess(DataAccess.Entities.ProductType productType)
        {
            return new ProductType(productTypeValidator)
            {
                ProductTypeId = productType.ProductTypeId,
                Name = productType.Name
            };
        }
    }
}
