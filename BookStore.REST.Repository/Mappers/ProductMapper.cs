﻿
using System;
using BookStore.Domain.Entities;
using BookStore.REST.Repository.Contracts.Mappers;
using FluentValidation;

namespace BookStore.REST.Repository.Mappers
{
    public class ProductMapper : IProductMapper
    {
        private readonly AbstractValidator<Product> productValidator;
        private readonly AbstractValidator<ProductType> productTypeValidator;

        public ProductMapper(AbstractValidator<Product> productValidator, AbstractValidator<ProductType> productTypeValidator)
        {
            this.productValidator = productValidator;
            this.productTypeValidator = productTypeValidator;
        }

        public DataAccess.Entities.Product FillDataAccessProductFromDomain(Domain.Entities.Product product)
        {
            DataAccess.Entities.Product result = new DataAccess.Entities.Product()
            {
                Name = product.Name,
                SKU = product.SKU,
                Quantity = product.Quantity,
                ProductTypeId = product.Type.ProductTypeId,
                Type = new DataAccess.Entities.ProductType()
            };

            result.ProductTypeId = product.Type.ProductTypeId;
            result.Type = null;
            
            return result;
        }
        
        public Domain.Entities.Product FillDomainProductFromDataAccess(DataAccess.Entities.Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            Product domainProduct = new Domain.Entities.Product(productValidator, productTypeValidator)
            {
                SKU = product.SKU,
                Name = product.Name,
                Type = new ProductType(productTypeValidator),
                Quantity = product.Quantity
            };

            domainProduct.Type = null;
            
            return domainProduct;
        }
    }
}

