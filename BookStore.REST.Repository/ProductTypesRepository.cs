﻿using System.Collections.Generic;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.Repository.Contracts;
using BookStore.REST.Repository.Contracts.Mappers;

namespace BookStore.REST.Repository
{
    public class ProductTypesRepository : BaseRepository, IProductTypesRepository
    {
        private readonly IProductTypeMapper productTypeMapper;
        private readonly AbstractListValidator<ProductTypesList, ProductType> productTypesListValidator;

        public ProductTypesRepository(IDataContext context, IProductTypeMapper productTypeMapper, AbstractListValidator<ProductTypesList, ProductType> productTypesListValidator)
            : base(context)
        {
            this.productTypeMapper = productTypeMapper;
            this.productTypesListValidator = productTypesListValidator;
        }

        public ProductTypesList GetProductTypes()
        {
            var productTypesList = new ProductTypesList(productTypesListValidator);

            foreach (var productType in DataContext.ProductTypes)
            {
                var domainProductType = productTypeMapper.FillDomainProductTypeFromDataAccess(productType);

                productTypesList.Add(domainProductType);
            }
            return productTypesList;
        }

        public void PersistProductTypes(IEnumerable<ProductType> productTypes)
        {
            foreach (var productType in productTypes)
            {
                DataContext.ProductTypes.Add(productTypeMapper.FillDataAccessProductTypeFromDomain(productType));
            }

            DataContext.SaveChanges();
        }
    }
}