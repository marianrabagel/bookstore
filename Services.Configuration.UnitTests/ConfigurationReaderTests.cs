﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Configuration.UnitTests.Fakes;
using Services.Configuration.UnitTests.DI;
using Services.Configuration.Contracts;
using Services.UnitTests;

namespace Services.Configuration.UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ConfigurationReaderTests : BaseTests<DependencyResolver>
    {
        private IConfigurationReader reader;

        [TestInitialize]
        public void SetUp()
        {
            reader = DependencyResolver.CreateInstance<IConfigurationReader>();
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(ArgumentNullException), typeof(Errors), "ErrArgumentNullExceptionMessage", "keyName")]
        public void TestThatPassingNullConfigurationKeyIsThrowingException()
        {
            reader.GetValueByKeyName<string>(null);
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(InvalidOperationException), "The key 'DummyKey' was not found in the configuration")]
        public void TestThatPassingUnknownConfigurationKeyIsThrowingException()
        {
            reader.GetValueByKeyName<string>(Constants.DummyKey);
        }

        [TestMethod]
        public void TestThatPassingAnApplicationSettingKeyIsGettingTheRightValue()
        {
            Assert.AreEqual(Constants.AppSettingValue, reader.GetValueByKeyName<string>(Constants.AppSettingKey));
        }

        [TestMethod]
        public void TestThatPassingAConnectionStringSettingKeyIsGettingTheRightValue()
        {
            Assert.AreEqual(Constants.ConnectionStringSettingValue, reader.GetValueByKeyName<string>(Constants.ConnectionStringSettingKey));
        }
    }
}
