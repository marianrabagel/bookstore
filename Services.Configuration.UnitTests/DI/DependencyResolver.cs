﻿using System.Diagnostics.CodeAnalysis;
using Autofac;
using Services.Configuration.DI.Contracts;
using Services.Configuration.UnitTests.Fakes;

namespace Services.Configuration.UnitTests.DI
{
    [ExcludeFromCodeCoverage]
    public class DependencyResolver : AbstractDependencyResolver
    {
        public override void PerformRegistrations()
        {
            ContainerBuilder.RegisterType<FakeConfigurationProvider>().As<Contracts.IConfigurationProvider>();
            ContainerBuilder.RegisterType<ConfigurationReader>().As<Contracts.IConfigurationReader>();

            base.PerformRegistrations();
        }
    }
}
