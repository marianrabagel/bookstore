﻿using Services.Configuration.Contracts;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Services.Configuration.UnitTests.Fakes
{
    [ExcludeFromCodeCoverage]
    public class FakeConfigurationProvider : IConfigurationProvider
    {
        private readonly Dictionary<string, string> repository;

        public FakeConfigurationProvider()
        {
            repository = new Dictionary<string, string>
            {
                {Constants.AppSettingKey, Constants.AppSettingValue},
                {Constants.ConnectionStringSettingKey, Constants.ConnectionStringSettingValue}
            };
        }

        public string GetApplicationSettings(string settingName)
        {
            return GetConnectionStrings(settingName);
        }

        public string GetConnectionStrings(string settingName)
        {
            return repository.ContainsKey(settingName) ? repository[settingName] : null;
        }
    }
}
