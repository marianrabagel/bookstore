﻿using System.Diagnostics.CodeAnalysis;

namespace Services.Configuration.UnitTests.Fakes
{
    [ExcludeFromCodeCoverage]
    public class Constants
    {
        public const string DummyKey = "DummyKey";
        public const string AppSettingKey = "appSetting";
        public const string AppSettingValue = "appSettingValue";
        public const string ConnectionStringSettingKey = "connectionStringSetting";
        public const string ConnectionStringSettingValue = "connectionStringSettingValue";
    }
}
