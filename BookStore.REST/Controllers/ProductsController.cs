﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BookStore.REST.BusinessLogic.Contracts;
using Newtonsoft.Json.Linq;

namespace BookStore.REST.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly IProductsImporter productsImporter;

        public ProductsController(IProductsImporter productsImporter)
        {
            this.productsImporter = productsImporter;
        }

        [Route("v1/products")]
        [HttpPost]
        public HttpResponseMessage Save(JArray jsonProductsArray)
        {
            using (HttpResponseMessage result = new HttpResponseMessage())
            {
                try
                {
                    string notificationMessage = productsImporter.DoWork(jsonProductsArray);

                    result.StatusCode = HttpStatusCode.Created;
                    result.ReasonPhrase = notificationMessage;
                }
                catch (Exception exception)
                {
                    result.StatusCode = HttpStatusCode.InternalServerError;
                    result.ReasonPhrase = exception.Message.Replace(Environment.NewLine, "  ");
                }

                return result;
            }
        }
    }
}
