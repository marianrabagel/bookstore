﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Autofac.Integration.WebApi;
using BookStore.REST.DI;

namespace BookStore.REST
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var typesArray = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(type => type.IsSubclassOf(typeof(ApiController)))
                .ToArray();

            var dependencyResolver = new DependencyResolver();
            dependencyResolver.PerformRegistrations(typesArray);

            var httpConfiguration = new HttpConfiguration
            {
                DependencyResolver = new AutofacWebApiDependencyResolver(dependencyResolver.Container)
            };

            WebApiConfig.Register(httpConfiguration);

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(dependencyResolver.Container);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}