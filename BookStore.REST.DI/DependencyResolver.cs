﻿using System;
using System.Diagnostics.CodeAnalysis;
using Autofac;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.BusinessLogic;
using BookStore.REST.BusinessLogic.Contracts;
using BookStore.REST.BusinessLogic.Contracts.Facades;
using BookStore.REST.BusinessLogic.Facades;
using BookStore.REST.DataAccess;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.Repository;
using BookStore.REST.Repository.Contracts;
using BookStore.REST.Repository.Contracts.Mappers;
using BookStore.REST.Repository.Mappers;
using FluentValidation;
using Services.Configuration;
using Services.Configuration.Contracts;
using Services.Configuration.DI.Contracts;
using Services.JsonConverter;
using Services.JsonConverter.Contracts;
using Services.JsonConverter.Contracts.Facades;
using Services.JsonConverter.Facades;

namespace BookStore.REST.DI
{
    [ExcludeFromCodeCoverage]
    public class DependencyResolver : AbstractDependencyResolver
    {
        public void PerformRegistrations(Type[] typesArray = null)
        {
            ContainerBuilder.RegisterType<ConfigurationProvider>().As<IConfigurationProvider>();
            ContainerBuilder.RegisterType<ConfigurationReader>().As<IConfigurationReader>();

            ContainerBuilder.RegisterType<ProductValidator>().As<AbstractValidator<Product>>();
            ContainerBuilder.RegisterType<ProductTypeValidator>().As<AbstractValidator<ProductType>>();
            ContainerBuilder.RegisterType<ProductsListValidator>()
                .As<AbstractListValidator<ProductsList, Product>>();
            ContainerBuilder.RegisterType<ProductTypesListValidator>()
                .As<AbstractListValidator<ProductTypesList, ProductType>>();
            ContainerBuilder.RegisterType<ProductValidationFacade>().As<IProductValidationFacade>()
                .PropertiesAutowired();

            ContainerBuilder.Register(c => new DataContext(GetConnectionString())).As<IDataContext>();
            ContainerBuilder.RegisterType<ProductTypesRepository>().As<IProductTypesRepository>();
            ContainerBuilder.RegisterType<ProductsRepository>().As<IProductsRepository>();
            ContainerBuilder.RegisterType<JsonToProductsConverter>().As<IJsonToProductsConverter>();
            ContainerBuilder.RegisterType<ProductsImporter>().As<IProductsImporter>();

            ContainerBuilder.RegisterType<ProductTypeMapper>().As<IProductTypeMapper>();

            ContainerBuilder.RegisterType<ProductsImporterFacade>().As<IProductsImporterFacade>()
                .PropertiesAutowired();

            if (typesArray != null)
            {
                ContainerBuilder.RegisterTypes(typesArray).PreserveExistingDefaults();
            }

            base.PerformRegistrations();
        }

        private string GetConnectionString()
        {
            return CreateInstance<IConfigurationReader>().GetValueByKeyName<string>("BookStore");
        }
    }
}

