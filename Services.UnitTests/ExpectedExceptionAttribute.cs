﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using System.Reflection;

namespace Services.UnitTests
{
    public sealed class ExpectedExceptionAttribute : ExpectedExceptionBaseAttribute
    {
        private Type originalExceptionType;
        private readonly Type expectedExceptionTypeLocal;
        private readonly string expectedExceptionMessageLocal;

        public ExpectedExceptionAttribute(Type expectedExceptionType)
            : this(expectedExceptionType, string.Empty)
        {
        }

        public ExpectedExceptionAttribute(Type expectedExceptionType, string expectedExceptionMessage)
        {
            expectedExceptionTypeLocal = expectedExceptionType;
            expectedExceptionMessageLocal = expectedExceptionMessage;
        }

        public ExpectedExceptionAttribute(Type expectedExceptionType, Type errorMessageResourceType,
            string errorMessageResourceName, params object[] args)
        {
            expectedExceptionTypeLocal = expectedExceptionType;

            var property = errorMessageResourceType.GetProperty(errorMessageResourceName,
                BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            if (property != null)
            {
                var getMethod = property.GetGetMethod(true);
                if (getMethod == null || !getMethod.IsAssembly && !getMethod.IsPublic)
                {
                    property = null;
                }
            }

            if (property == null)
            {
                throw new InvalidOperationException(string.Format(Errors.ErrInvalidStaticProperty,
                    errorMessageResourceType.FullName, errorMessageResourceName));
            }
            if (property.PropertyType != typeof(string))
            {
                throw new InvalidOperationException(string.Format(Errors.ErrInvalidPropertyType,
                    property.Name, errorMessageResourceType.FullName));
            }

            var val = (string)property.GetValue(null, null);

            if (!string.IsNullOrEmpty(val) && args != null)
            {
                expectedExceptionMessageLocal = string.Format(val, args);
            }
            else
            {
                expectedExceptionMessageLocal = val;
            }
        }

        protected override void Verify(Exception exception)
        {
            originalExceptionType = exception.GetType();

            AssertException(exception);
        }

        private void AssertException(Exception exception)
        {
            if (exception == null)
            {
                Assert.Fail(string.Format(CultureInfo.InvariantCulture,
                    "Expected:<{0}>. Actual:<{1}>. {2}", originalExceptionType, expectedExceptionTypeLocal,
                    Errors.ErrWrongExceptionWasThrown));
            }

            if (IsInstanceOfExpectedException(exception))
            {
                Assert.IsInstanceOfType(exception, expectedExceptionTypeLocal,
                    Errors.ErrWrongExceptionWasThrown);

                if (string.IsNullOrWhiteSpace(expectedExceptionMessageLocal))
                {
                    Assert.Fail(string.Format(CultureInfo.InvariantCulture,
                        "Expected:<{0}>. Actual:<{1}>. {2}", "Empty", exception.Message,
                        Errors.ErrWrongExceptionMessage));
                }

                Assert.AreEqual(expectedExceptionMessageLocal, exception.Message, Errors.ErrWrongExceptionMessage);

                return;
            }

            AssertException(exception.InnerException);
        }

        private bool IsInstanceOfExpectedException(Exception exception)
        {
            return exception.GetType() == expectedExceptionTypeLocal;
        }
    }
}

