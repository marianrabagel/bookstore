﻿using System.Diagnostics.CodeAnalysis;
using Services.Configuration.DI.Contracts;

namespace Services.UnitTests
{
    [ExcludeFromCodeCoverage]
    public abstract class BaseTests<TResolver> where TResolver : AbstractDependencyResolver, new()
    {
        private TResolver dependencyResolver;

        public TResolver DependencyResolver
        {
            get
            {
                if (dependencyResolver != null)
                {
                    return dependencyResolver;
                }

                dependencyResolver = new TResolver();
                dependencyResolver.PerformRegistrations();
                return dependencyResolver;
            }
        }
    }
}
