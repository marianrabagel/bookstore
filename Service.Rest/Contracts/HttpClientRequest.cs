﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Services.Rest.Contracts
{
    public abstract class HttpClientRequest
    {
        protected HttpClientRequest()
        {
        }

        public abstract Task<JObject> GetAsync(string uri);

        public abstract Task<JArray> GetArrayAsync(string uri);

        public abstract Task<JObject> PostAsync(string uri, object jObjectToPost);

        protected static JObject GetDomainEntityResponseFromJson(HttpResponseMessage response)
        {
            JObject returnJobject = null;

            if (response == null)
            {
                ThrowInvalidOperationException("empty response");
            }

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created)
            {
                ThrowInvalidOperationException(response.StatusCode.ToString());
            }

            try
            {
                returnJobject = ExtractDomainEntity(JObject.Parse(GetResponseAsString(response)));
            }
            catch (Exception)
            {
                ThrowInvalidOperationException("bad response");
            }

            return returnJobject;
        }

        protected static JArray GetArrayResponseFromJson(HttpResponseMessage response)
        {
            JArray returnJobject = null;

            if (response == null)
            {
                ThrowInvalidOperationException("empty response");
            }

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created)
            {
                ThrowInvalidOperationException(response.StatusCode.ToString());
            }

            try
            {
                returnJobject = JArray.Parse(GetResponseAsString(response));
            }
            catch (Exception)
            {
                ThrowInvalidOperationException("bad response");
            }

            return returnJobject;
        }

        private static JObject ExtractDomainEntity(JObject parsedJObject)
        {
            return parsedJObject["domainEntity"] != null
                ? parsedJObject["domainEntity"].Value<JObject>()
                : parsedJObject;
        }

        private static string GetResponseAsString(HttpResponseMessage response)
        {
            string responseAsString;

            using (var stream = response.Content.ReadAsStreamAsync().Result)
            {
                if (stream == null)
                {
                    ThrowInvalidOperationException("empty response");
                }

                responseAsString = GetResponseAsString(stream);
            }

            return responseAsString;
        }

        private static string GetResponseAsString(Stream stream)
        {
            string responseAsString;
            using (var sr = new StreamReader(stream, Encoding.UTF8))
            {
                responseAsString = sr.ReadToEnd();
            }
            return responseAsString;
        }

        private static void ThrowInvalidOperationException(string responseStatus, string reason = "")
        {
            var message = new StringBuilder();
            message.AppendFormat(CultureInfo.CurrentCulture, "Failed to process request: '{0}'.", responseStatus);
            if (!string.IsNullOrWhiteSpace(reason))
            {
                message.AppendLine();
                message.AppendFormat(CultureInfo.CurrentCulture, "Reason: '{0}'.", reason);
            }

            throw new InvalidOperationException(message.ToString());
        }
    }
}