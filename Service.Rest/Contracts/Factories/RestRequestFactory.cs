﻿using System;

namespace Services.Rest.Contracts.Factories
{
    public abstract class RestRequestFactory
    {
        public abstract RestRequest CreateGetRequest(Uri uri);
        public abstract RestRequest CreateSaveRequest(Uri uri, object dataToSave);
        public abstract RestRequest CreateUpdateRequest(Uri uri, object dataForUpdate);
        public abstract RestRequest CreateDeleteRequest(Uri uri);
    }
}