﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Services.Rest.Contracts
{
    public abstract class RestRequest
    {
        public Uri RequestUri { get; protected set; }
        public HttpVerbs HttpVerbs { get; protected set; }
        public HttpStatusCode HttpStatusCode { get; protected set; }

        protected RestRequest()
        {
            ServicePointManager.DefaultConnectionLimit = 1000;
        }

        public object Payload { get; set; }

        public abstract Task<JObject> InvokeAsync();

        protected HttpWebRequest CreateWebRequest()
        {
            var webRequest = WebRequest.CreateHttp(RequestUri);

            webRequest.Method = HttpVerbs.ToString().ToUpper();
            webRequest.ContentType = "application/json; charset=utf-8";
            webRequest.Accept = "application/json";
            webRequest.AuthenticationLevel = AuthenticationLevel.MutualAuthRequested;
            webRequest.UseDefaultCredentials = true;
            webRequest.ImpersonationLevel = TokenImpersonationLevel.Delegation;
            webRequest.KeepAlive = false;
            webRequest.Timeout = 50000;
            webRequest.CookieContainer = new CookieContainer();

            AddPayloadToWebRequest(webRequest);

            return webRequest;
        }

        private void AddPayloadToWebRequest(WebRequest webRequest)
        {
            if (Payload == null)
            {
                return;
            }

            var payloadAsString = JsonConvert.SerializeObject(Payload);
            var byteArray = Encoding.UTF8.GetBytes(payloadAsString);

            using (var requestStream = webRequest.GetRequestStream())
            {
                requestStream.Write(byteArray, 0, byteArray.Length);

                requestStream.Flush();
                requestStream.Close();
            }
        }

        protected async Task<JObject> GetResponseAsync(WebRequest httpRequest, HttpStatusCode? expectedStatusCode = null)
        {
            HttpWebResponse httpWebResponse = null;

            try
            {
                httpWebResponse = (HttpWebResponse)await httpRequest.GetResponseAsync().ConfigureAwait(false);

                if (httpWebResponse == null)
                {
                    ThrowInvalidOperationException("empty response");
                }

                if ((expectedStatusCode.HasValue && expectedStatusCode.Value != httpWebResponse.StatusCode) ||
                    (!expectedStatusCode.HasValue && httpWebResponse.StatusCode >= HttpStatusCode.BadRequest))
                {
                    ThrowInvalidOperationException(httpWebResponse.StatusDescription);
                }

                return GetDomainEntityResponseFromJson(httpWebResponse);
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    throw;
                }
                httpWebResponse = (HttpWebResponse)e.Response;

                if (expectedStatusCode != httpWebResponse.StatusCode)
                {
                    JObject response = null;

                    try
                    {
                        response = GetDomainEntityResponseFromJson(httpWebResponse);
                    }
                    catch
                    {
                        ThrowInvalidOperationException("bad response");
                    }

                    if (response != null)
                    {
                        var reason = "Unknown";

                        if (response["error"] != null)
                        {
                            reason = response["error"].ToString(Formatting.None);
                        }
                        else if (response["message"] != null)
                        {
                            reason = response["message"].ToString(Formatting.None);
                        }

                        ThrowInvalidOperationException(httpWebResponse.StatusDescription, reason);
                    }
                }
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                    httpWebResponse.Dispose();
                }
            }

            ThrowInvalidOperationException("empty response");
            return null;
        }

        private JObject GetDomainEntityResponseFromJson(WebResponse response)
        {
            string responseAsString;
            using (var stream = response.GetResponseStream())
            {
                if (stream == null)
                {
                    ThrowInvalidOperationException("empty response");
                }

                using (var sr = new StreamReader(stream, Encoding.UTF8))
                {
                    responseAsString = sr.ReadToEnd();
                }
            }

            var parsedJObject = JObject.Parse(responseAsString);

            return parsedJObject["domainEntity"] != null
                ? parsedJObject["domainEntity"].Value<JObject>()
                : parsedJObject;
        }

        private static void ThrowInvalidOperationException(string responseStatus, string reason = "")
        {
            var message = new StringBuilder();
            message.AppendFormat(CultureInfo.CurrentCulture, "Failed to process request: '{0}'.", responseStatus);
            if (!string.IsNullOrWhiteSpace(reason))
            {
                message.AppendLine();
                message.AppendFormat(CultureInfo.CurrentCulture, "Reason: '{0}'.", reason);
            }

            throw new InvalidOperationException(message.ToString());
        }
    }
}