﻿using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Services.Rest.Contracts;

namespace Services.Rest
{
    public class AnonymousAuthenticatedRestRequest : RestRequest
    {
        public AnonymousAuthenticatedRestRequest(Uri requestUri, HttpVerbs httpVerbs = HttpVerbs.Get, HttpStatusCode httpStatusCode = HttpStatusCode.OK)
        {
            if (requestUri == null)
            {
                throw new InvalidOperationException("requestUri cannot be null");
            }

            RequestUri = requestUri;
            HttpVerbs = httpVerbs;
            HttpStatusCode = httpStatusCode;
        }

        public override async Task<JObject> InvokeAsync()
        {
            var webRequest = CreateWebRequest();

            var jObject = await GetResponseAsync(webRequest, HttpStatusCode);

            webRequest.Abort();

            return jObject;
        }
    }
}
