﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Services.Rest.Contracts;

namespace Services.Rest
{
    public class AnonymousHttpClientRequest : HttpClientRequest
    {
        public override async Task<JObject> GetAsync(string uri)
        {
            if (string.IsNullOrWhiteSpace(uri))
            {
                throw new ArgumentException("uri");
            }

            using (var client = GetHttpClient())
            {
                using (var result = await client.GetAsync(uri))
                {
                    return GetDomainEntityResponseFromJson(result);
                }
            }
        }

        public override async Task<JArray> GetArrayAsync(string uri)
        {
            if (string.IsNullOrWhiteSpace(uri))
            {
                throw new ArgumentException("uri");
            }

            using (var client = GetHttpClient())
            {
                using (var result = await client.GetAsync(uri))
                {
                    return GetArrayResponseFromJson(result);
                }
            }
        }

        public override async Task<JObject> PostAsync(string uri, object jObjectToPost)
        {
            if (string.IsNullOrWhiteSpace(uri))
            {
                throw new ArgumentException("uri");
            }

            if (jObjectToPost == null)
            {
                throw new ArgumentException("jObjectToPost");
            }

            using (var client = GetHttpClient())
            {
                using (var result = await client.PostAsJsonAsync(uri, jObjectToPost))
                {
                    return GetDomainEntityResponseFromJson(result);
                }
            }
        }

        private static HttpClient GetHttpClient()
        {
            var httpClient = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("keep-alive", "timeout: 60000");
            httpClient.Timeout = TimeSpan.FromMinutes(10);

            return httpClient;
        }
    }
}