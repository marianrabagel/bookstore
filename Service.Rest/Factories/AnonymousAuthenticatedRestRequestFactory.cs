﻿using System;
using System.Net;
using Services.Rest.Contracts;
using Services.Rest.Contracts.Factories;

namespace Services.Rest.Factories
{
    public class AnonymousAuthenticatedRestRequestFactory : RestRequestFactory
    {
        public override RestRequest CreateGetRequest(Uri uri)
        {
            return new AnonymousAuthenticatedRestRequest(uri);
        }

        public override RestRequest CreateSaveRequest(Uri uri, object dataToSave)
        {
            return new AnonymousAuthenticatedRestRequest(uri, HttpVerbs.Post, HttpStatusCode.Created)
            {
                Payload = dataToSave
            };
        }

        public override RestRequest CreateUpdateRequest(Uri uri, object dataForUpdate)
        {
            return new AnonymousAuthenticatedRestRequest(uri, HttpVerbs.Put)
            {
                Payload = dataForUpdate
            };
        }

        public override RestRequest CreateDeleteRequest(Uri uri)
        {
            return new AnonymousAuthenticatedRestRequest(uri, HttpVerbs.Delete, HttpStatusCode.ResetContent);
        }
    }
}
