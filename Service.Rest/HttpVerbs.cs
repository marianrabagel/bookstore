﻿using System.ComponentModel;

namespace Services.Rest
{
    public enum HttpVerbs
    {
        [Description("Retrieves the information or entity that is identified by the URI of the request.")]
        Get,

        [Description("Posts a new entity as an addition to a URI.")]
        Post,

        [Description("Replaces an entity that is identified by a URI.")]
        Put,

        [Description("Requests that a specified URI be deleted.")]
        Delete
    }
}
