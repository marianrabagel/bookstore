﻿using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.REST.BusinessLogic;
using BookStore.REST.BusinessLogic.Contracts.Facades;
using BookStore.REST.BusinessLogic.Facades;
using BookStore.REST.Controllers;
using BookStore.REST.IntegrationTests.DI;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets;
using BookStore.REST.Repository;
using BookStore.REST.Repository.Contracts;
using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Services.JsonConverter;
using Services.JsonConverter.Contracts.Facades;
using Services.JsonConverter.Facades;
using Services.UnitTests;

namespace BookStore.REST.IntegrationTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ImportProductsTest : BaseTests<DependencyResolver>
    {
        private JArray products;
        private ProductsRepository productsRepository;
        private ProductTypesRepository productTypesRepository;
        
        [TestInitialize]
        public void Setup()
        {
            products = new JArray();
            productsRepository = (ProductsRepository) DependencyResolver.CreateInstance<IProductsRepository>();
            productTypesRepository = (ProductTypesRepository) DependencyResolver.CreateInstance<IProductTypesRepository>();
        }

        [TestMethod]
        public void TestThatProductsFromAValidListWillBeImportedToTheDatabase()
        {
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct1());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct2());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct3());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct4Has0Quantity());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct5());

            var productsInitialCount = productsRepository.GetProducts().Count;
            var productTypesInitialCount = productTypesRepository.GetProductTypes().Count;

            var importController = new ProductsController(GetProductsImporter());
            var response = importController.Save(products);

            var productsCount = productsRepository.GetProducts().Count;
            var productTypesCount = productTypesRepository.GetProductTypes().Count;

            Assert.IsTrue(response.StatusCode == HttpStatusCode.Created);

            Assert.AreEqual(0, productsInitialCount);
            Assert.AreEqual(4, productsCount);

            Assert.AreEqual(0, productTypesInitialCount);
            Assert.AreEqual(2, productTypesCount);
        }

        [TestCleanup]
        public void TearDown()
        {
            CleanDataBase();
        }


        private void CleanDataBase()
        {
            ((DbSet<DataAccess.Entities.Product>) productsRepository.DataContext.Products)
                .RemoveRange(productsRepository.DataContext.Products.ToList());
            productsRepository.DataContext.SaveChanges();

            ((DbSet<DataAccess.Entities.ProductType>)productTypesRepository.DataContext.ProductTypes)
                .RemoveRange(productTypesRepository.DataContext.ProductTypes.ToList());
            productTypesRepository.DataContext.SaveChanges();
        }

        private void AddJtokenToJsonProductsArray(Product domainProduct)
        {
            var product = JsonConvert.SerializeObject(domainProduct);
            JToken jtoken1 = JToken.Parse(product);
            products.Add(jtoken1);
        }

        private ProductsImporter GetProductsImporter()
        {
            IProductValidationFacade productValidationFacade = new ProductValidationFacade()
            {
                ProductValidator = DependencyResolver.CreateInstance<AbstractValidator<Product>>(),
                ProductTypeValidator = DependencyResolver.CreateInstance<AbstractValidator<ProductType>>(),
                ProductsListValidator = DependencyResolver.CreateInstance<AbstractListValidator<ProductsList, Product>>()
            };
            IProductsImporterFacade productsImporterFacade = new ProductsImporterFacade()
            {
                JsonToProductsConverter = new JsonToProductsConverter(productValidationFacade),
                ProductsRepository = productsRepository,
                ProductTypesRepository = productTypesRepository
            };
            ProductsImporter productsImporter = new ProductsImporter(productsImporterFacade);

            return productsImporter;
        }
    }
}
