﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Autofac;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using FluentValidation;
using log4net;
using Services.Configuration;
using Services.Configuration.Contracts;
using Services.Configuration.DI.Contracts;
using Services.CSVImport;
using Services.CSVImport.Contracts;
using Services.CSVImport.Contracts.Facades;
using Services.CSVImport.Facades;
using Services.Rest.Contracts.Factories;
using Services.Rest.Factories;

namespace BookStore.DI
{
    [ExcludeFromCodeCoverage]
    public class DependencyResolver : AbstractDependencyResolver
    {
        private IConfigurationReader configurationReader;

        public override void PerformRegistrations()
        {
            ContainerBuilder.RegisterType<ConfigurationProvider>().As<IConfigurationProvider>();
            ContainerBuilder.RegisterType<ConfigurationReader>().As<IConfigurationReader>();

            ContainerBuilder.RegisterType<ProductsListValidator>().As<AbstractListValidator<ProductsList, Product>>();
            ContainerBuilder.RegisterType<ProductValidator>().As<AbstractValidator<Product>>();
            ContainerBuilder.RegisterType<ProductTypeValidator>().As<AbstractValidator<ProductType>>();

            ContainerBuilder.Register(c => new FileStream(GetConfiguredFilePath(), FileMode.Open, FileAccess.Read)).As<Stream>();

            ContainerBuilder.RegisterType<CSVImportProvider>().As<ICSVImportProvider>();
            ContainerBuilder.RegisterType<CSVImportReader>().As<ICSVImportReader>();

            ContainerBuilder.Register(x => LogManager.GetLogger("Program")).As<ILog>().SingleInstance();

            ContainerBuilder.RegisterType<CSVImportReaderFacade>().As<ICSVImportReaderFacade>().PropertiesAutowired();

            ContainerBuilder.RegisterType<AnonymousAuthenticatedRestRequestFactory>().As<RestRequestFactory>();

            base.PerformRegistrations();
        }

        private string GetConfiguredFilePath()
        {
            var directoryInfo = new DirectoryInfo(DataFolder);
            if (!directoryInfo.Exists)
            {
                throw new InvalidOperationException("Data folder doesn't exists");
            }

            return Path.Combine(DataFolder, FileName);
        }

        private string DataFolder => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationReader.GetValueByKeyName<string>("DataFolderName"));

        private string FileName => ConfigurationReader.GetValueByKeyName<string>("DataFileName");

        private IConfigurationReader ConfigurationReader => configurationReader ?? (configurationReader = CreateInstance<IConfigurationReader>());
    }
}
