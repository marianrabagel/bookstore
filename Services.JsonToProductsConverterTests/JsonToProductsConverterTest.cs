﻿
using System.Diagnostics.CodeAnalysis;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Services.JsonConverter;
using Services.JsonConverter.Contracts;
using Services.JsonConverter.Contracts.Facades;
using Services.JsonConverter.Facades;
using Services.JsonToProductsConverterTests.DI;
using Services.UnitTests;
using DataSets = BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets.DataSets;

namespace Services.JsonToProductsConverterTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class JsonToProductsConverterTest : BaseTests<DependencyResolver>
    {
        private IJsonToProductsConverter jsonToProductsConverter;
        private IProductValidationFacade productValidationFacade;

        [TestInitialize]
        public void Setup()
        {
            productValidationFacade = new ProductValidationFacade()
            {
                ProductValidator = new ProductValidator(),
                ProductTypeValidator = new ProductTypeValidator(),
                ProductsListValidator = new ProductsListValidator()
            };
            jsonToProductsConverter = new JsonToProductsConverter(productValidationFacade);
        }

        [TestMethod]
        public void TestThatAJsonProductListIsConvertedToDomainProductList()
        {
            var product = JsonConvert.SerializeObject(DataSets.GetValidDomainProduct1());
            JToken jtoken1 = JToken.Parse(product);
            product = JsonConvert.SerializeObject(DataSets.GetValidDomainProduct2());
            JToken jtoken2 = JToken.Parse(product);

            JArray jarray = new JArray
            {
                jtoken1,
                jtoken2
            };
            ProductsList productsList = jsonToProductsConverter.GetProductsListFromJson(jarray);

            Assert.AreEqual(2, productsList.Count);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Name, productsList[0].Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().SKU, productsList[0].SKU);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Quantity, productsList[0].Quantity);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Type.Name, productsList[0].Type.Name);

            Assert.AreEqual(DataSets.GetValidDomainProduct2().Name, productsList[1].Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().SKU, productsList[1].SKU);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Quantity, productsList[1].Quantity);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Type.Name, productsList[1].Type.Name);

        }

        [TestMethod]
        public void TestThatAJsonEmptyListIsTransformedIntoAnDomainEmptyList()
        {
            JArray jarray = new JArray();

            ProductsList productsList = jsonToProductsConverter.GetProductsListFromJson(jarray);
            Assert.AreEqual(0, productsList.Count);
        }
    }
}
