﻿using System.Collections.Generic;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.Repository.Contracts;

namespace BookStore.REST.Repositories.UnitTests.BussinessLogic.Fakes
{
    public class FakeProductTypeRepository : IProductTypesRepository
    {
        public ProductTypesList Data;

        public FakeProductTypeRepository()
        {
            Data = new ProductTypesList(new ProductTypesListValidator());
        }

        public void PersistProductTypes(IEnumerable<ProductType> productTypes)
        {
            foreach (var productType in productTypes)
            {
                Data.Add(productType);
            }
        }

        public ProductTypesList GetProductTypes()
        {
            return (ProductTypesList) Data;
        }

        public void Add(ProductType productType)
        {
            Data.Add(productType);
        }
    }
}
