﻿using BookStore.Domain.Entities;
using BookStore.REST.Repository.Contracts;
using BookStore.Domain.Validators;

namespace BookStore.REST.Repositories.UnitTests.BussinessLogic.Fakes
{
    public class FakeProductRepository : IProductsRepository
    {
        public ProductsList Data;

        public FakeProductRepository()
        {
            Data = new ProductsList(new ProductsListValidator());
        }

        public FakeProductRepository(Product product)
        {
            Data = new ProductsList(new ProductsListValidator())
            {
                product
            };
        }
        
        public void PersistProductsImportData(ProductsList productsList)
        {
            Data = new ProductsList(new ProductsListValidator());

            foreach (var product in productsList)
            {
                Data.Add(product);
            }
        }

        public ProductsList GetProducts()
        {
            return Data;
        }

        public void Add(Product product)
        {
            Data.Add(product);
        }
    }
}
