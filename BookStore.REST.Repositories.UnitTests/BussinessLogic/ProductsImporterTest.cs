﻿using System;
using System.Diagnostics.CodeAnalysis;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.BusinessLogic;
using BookStore.REST.BusinessLogic.Contracts;
using BookStore.REST.BusinessLogic.Contracts.Facades;
using BookStore.REST.BusinessLogic.Facades;
using BookStore.REST.Repositories.UnitTests.BussinessLogic.DI;
using BookStore.REST.Repositories.UnitTests.BussinessLogic.Fakes;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets;
using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Services.JsonConverter;
using Services.JsonConverter.Facades;
using Services.UnitTests;

namespace BookStore.REST.Repositories.UnitTests.BussinessLogic
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ProductsImporterTest : BaseTests<DependencyResolver>
    {
        private IProductsImporterFacade productsImporterFacade;
        private IProductsImporter productsImporter;
        private JArray jsonProductsArray;
        FakeProductTypeRepository fakeProductTypeRepsitory;
        FakeProductRepository fakeProductRepository;

        [TestInitialize]
        public void SetUp()
        {
            productsImporterFacade = DependencyResolver.CreateInstance<IProductsImporterFacade>();
            productsImporter = DependencyResolver.CreateInstance<IProductsImporter>();

            jsonProductsArray = new JArray();
            fakeProductTypeRepsitory = new FakeProductTypeRepository();
            fakeProductRepository = new FakeProductRepository();
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(ValidationException), @"Validation failed: 
 -- 'BookStore.Domain.Entities.ProductsList' should not be empty.")]
        public void TestThatImportingFromAnEmptyJsonProductsArrayThrowsException()
        {
            CreateProductImporter();
            productsImporter.DoWork(jsonProductsArray);
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(ArgumentNullException), @"Value cannot be null.
Parameter name: jsonProductsArray")]
        public void TestThatImportingFromNullJsonProductsArrayThrowsException()
        {
            CreateProductImporter();
            productsImporter.DoWork(null);
        }

        [TestMethod]
        public void TestThatImporting2ValidProductsWillSaveThoseProductsAndTypes()
        {
            CreateProductImporter();
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct1());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct2());

            productsImporter.DoWork(jsonProductsArray);
            var productsList = productsImporterFacade.ProductsRepository.GetProducts();
            var productTypesList = productsImporterFacade.ProductTypesRepository.GetProductTypes();

            Assert.AreEqual(2, productTypesList.Count);
            Assert.AreEqual(2, productsList.Count);

            Assert.AreEqual(DataSets.GetValidDomainProduct1().Name, productsList[0].Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().SKU, productsList[0].SKU);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Quantity, productsList[0].Quantity);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Type.Name, productsList[0].Type.Name);

            Assert.AreEqual(DataSets.GetValidDomainProduct2().Name, productsList[1].Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().SKU, productsList[1].SKU);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Quantity, productsList[1].Quantity);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Type.Name, productsList[1].Type.Name);

            Assert.AreEqual(DataSets.GetValidDomainProduct1().Type.Name, productTypesList[0].Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Type.Name, productTypesList[1].Name);
        }

        [TestMethod]
        public void TestThatQuantity0BusinessRulesAppliesOnAlistWithQuantities0()
        {
            CreateProductImporter();
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct4Has0Quantity());

            productsImporter.DoWork(jsonProductsArray);
            var productsCount = productsImporterFacade.ProductsRepository.GetProducts().Count;
            var productTypesCount = productsImporterFacade.ProductTypesRepository.GetProductTypes().Count;

            Assert.AreEqual(0, productTypesCount);
            Assert.AreEqual(0, productsCount);
        }

        [TestMethod]
        public void TestThatAddingProductsWithTheSameSkuNameAndTypeWillUpdateTheQuantity()
        {
            fakeProductRepository.Add(DataSets.GetValidDomainProduct1());
            fakeProductTypeRepsitory.Add(DataSets.GetValidDomainProductType1());
            CreateProductImporter();
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct1());

            productsImporter.DoWork(jsonProductsArray);
            var productsList = productsImporterFacade.ProductsRepository.GetProducts();
            var productTypesCount = productsImporterFacade.ProductTypesRepository.GetProductTypes().Count;

            Assert.AreEqual(1, productsList.Count);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Quantity * 2, productsList[0].Quantity);

            Assert.AreEqual(1, productTypesCount);
        }

        [TestMethod]
        public void TestThatIfSkuDuplicateWithDifferenteNameAndTypeAreFoundViolatesBussinessRuleAndNotifiesUserANdImportsTheOthers()
        {
            fakeProductRepository.Add(DataSets.GetValidDomainProduct2());
            fakeProductTypeRepsitory.Add(DataSets.GetValidDomainProductType2());

            CreateProductImporter();

            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProductWithDuplicateSkuWithProduct2AndDifferentNameAndType());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct1());
            productsImporter.DoWork(jsonProductsArray);
            var productsList = productsImporterFacade.ProductsRepository.GetProducts();
            var productTypesList = productsImporterFacade.ProductTypesRepository.GetProductTypes();

            Assert.AreEqual(2, productsList.Count);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().SKU, productsList[0].SKU);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Name, productsList[0].Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Quantity, productsList[0].Quantity);
            Assert.AreEqual(DataSets.GetValidDomainProduct2().Type.Name.ToUpper(), productsList[0].Type.Name);

            Assert.AreEqual(DataSets.GetValidDomainProduct1().SKU, productsList[1].SKU);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Name, productsList[1].Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Quantity, productsList[1].Quantity);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().Type.Name, productsList[1].Type.Name);
            Assert.AreEqual(DataSets.GetValidDomainProduct1().SKU, productsList[1].SKU);

            Assert.AreEqual(2, productTypesList.Count);

            Assert.AreEqual(DataSets.GetValidDomainProductType2().Name, productTypesList[0].Name);
            Assert.AreEqual(DataSets.GetValidDomainProductType1().Name, productTypesList[1].Name);

        }

        [TestMethod]
        public void TestThatIfSkuDuplicateWithDifferenteNameAndTypeAreFoundViolatesBussinessRuleAndNotifiesUser()
        {
            fakeProductRepository.Add(DataSets.GetValidDomainProduct2());
            fakeProductTypeRepsitory.Add(DataSets.GetValidDomainProductType2());

            CreateProductImporter();

            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProductWithDuplicateSkuWithProduct2AndDifferentNameAndType());
            string message = productsImporter.DoWork(jsonProductsArray);

            string errorMessage = "There is a product with the same SKU and different Name and Type";
            Assert.AreEqual(errorMessage, message);

            Assert.AreEqual(1, productsImporterFacade.ProductsRepository.GetProducts().Count);
            Assert.AreEqual(1, productsImporterFacade.ProductTypesRepository.GetProductTypes().Count);


        }

        [TestMethod]
        public void TestThatAddingTwoProductsWIthTheSameTypeWillSaveOnlyOneType()
        {
            CreateProductImporter();

            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct1());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct3());
            var productTypesInitialCount = productsImporterFacade.ProductTypesRepository.GetProductTypes().Count;
            var productsInitialCount = productsImporterFacade.ProductsRepository.GetProducts().Count;

            productsImporter.DoWork(jsonProductsArray);

            var productTypesCount  = productsImporterFacade.ProductTypesRepository.GetProductTypes().Count;
            var productsCount = productsImporterFacade.ProductsRepository.GetProducts().Count;

            Assert.AreEqual(0, productTypesInitialCount);
            Assert.AreEqual(1, productTypesCount);

            Assert.AreEqual(0, productsInitialCount);
            Assert.AreEqual(2, productsCount);
        }

        [TestMethod]
        public void TestThatAddingAProductWith0QuantityWillNotSaveHisType()
        {
            CreateProductImporter();
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct4Has0Quantity());

            productsImporter.DoWork(jsonProductsArray);

            var productsCount = productsImporterFacade.ProductsRepository.GetProducts().Count;
            var productTypesCount = productsImporterFacade.ProductTypesRepository.GetProductTypes().Count;
            Assert.AreEqual(0, productsCount);
            Assert.AreEqual(0, productTypesCount);
        }

        [TestMethod]
        public void TestThatAddingAProductWithTheSameProductTypeNameDifferentCaseProductTypeNameIsNotAdded()
        {
            fakeProductRepository.Add(DataSets.GetValidDomainProduct1());
            fakeProductTypeRepsitory.Add(DataSets.GetValidDomainProductType1());
            CreateProductImporter();
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct6());

            productsImporter.DoWork(jsonProductsArray);

            var productsCount = productsImporterFacade.ProductsRepository.GetProducts().Count;
            var productTypesCount = productsImporterFacade.ProductTypesRepository.GetProductTypes().Count;
            Assert.AreEqual(2, productsCount);
            Assert.AreEqual(1, productTypesCount);
        }

        private void AddJtokenToJsonProductsArray(Product domainProduct)
        {
            var product = JsonConvert.SerializeObject(domainProduct);
            JToken jtoken1 = JToken.Parse(product);
            jsonProductsArray.Add(jtoken1);
        }

        private void CreateProductImporter()
        {
            var productValidationFacade = new ProductValidationFacade()
            {
                ProductValidator = new ProductValidator(),
                ProductTypeValidator = new ProductTypeValidator(),
                ProductsListValidator = new ProductsListValidator()
            };
            productsImporterFacade = new ProductsImporterFacade
            {
                ProductTypesRepository = fakeProductTypeRepsitory,
                ProductsRepository = fakeProductRepository,
                JsonToProductsConverter = new JsonToProductsConverter(productValidationFacade)
            };
            productsImporter = new ProductsImporter(productsImporterFacade);
        }
    }

}
