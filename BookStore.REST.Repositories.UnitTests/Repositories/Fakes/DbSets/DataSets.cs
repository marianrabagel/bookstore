﻿using System.Diagnostics.CodeAnalysis;
using BookStore.Domain.Validators;
using BookStore.REST.DataAccess.Entities;
using ProductType = BookStore.REST.DataAccess.Entities.ProductType;

namespace BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets
{
    [ExcludeFromCodeCoverage]
    public static class DataSets
    {
        public static ProductType GetValidDataAccessProductType1()
        {
            var productType = new ProductType()
            {
                Name = "Book",
                ProductTypeId = 1
            };

            return productType;
        }

        public static ProductType GetValidDataAccessProductType2()
        {
            var productType = new ProductType()
            {
                Name = "CD",
                ProductTypeId = 2
            };

            return productType;
        }

        public static Product GetValidDataAccessProduct1()
        {
            return new Product()
            {
                SKU = "F-M7ncpy",
                Name = "Fairytale 1",
                Type = GetValidDataAccessProductType1(),
                Quantity = 23,
            };
        }

        public static Product GetValidDataAccessProduct2()
        {
            return new Product()
            {
                SKU = "F-4JshmZ",
                Name = "Album 2",
                Type = GetValidDataAccessProductType2(),
                Quantity = 2
            };
        }
        
        public static Domain.Entities.Product GetValidDomainProduct1()
        {
            return new Domain.Entities.Product(new ProductValidator(), new ProductTypeValidator())
            {
                SKU = "F-M7ncpy",
                Name = "Fairytale 1",
                Type = GetValidDomainProductType1(),
                Quantity = 23
            };
        }
        
        public static Domain.Entities.ProductType GetValidDomainProductType1()
        {
            return new Domain.Entities.ProductType(new ProductTypeValidator())
            {
                Name = "BOOK",
                ProductTypeId = 1
            };
        }

        public static Domain.Entities.Product GetValidDomainProduct2()
        {
            return new Domain.Entities.Product(new ProductValidator(), new ProductTypeValidator())
            {
                SKU = "F-4JshmZ",
                Name = "Album 2",
                Type = GetValidDomainProductType2(),
                Quantity = 2
            };
        }

        public static Domain.Entities.ProductType GetValidDomainProductType2()
        {
            return new Domain.Entities.ProductType(new ProductTypeValidator())
            {
                Name = "CD",
                ProductTypeId = 2
            };
        }

        public static Domain.Entities.Product GetValidDomainProduct3()
        {
            return new Domain.Entities.Product(new ProductValidator(), new ProductTypeValidator())
            {
                SKU = "F-qdS7HZ",
                Name = "Novel 3",
                Type = GetValidDomainProductType3(),
                Quantity = 14
            };
        }

        private static Domain.Entities.ProductType GetValidDomainProductType3()
        {
            return new Domain.Entities.ProductType(new ProductTypeValidator())
            {
                Name = "BOOK",
                ProductTypeId = 1
            };
        }
        
        public static Domain.Entities.Product GetValidDomainProduct5()
        {
            return new Domain.Entities.Product(new ProductValidator(), new ProductTypeValidator())
            {
                SKU = "F-8nQw53",
                Name = "Training Material 5",
                Type = GetValidDomainProductType5(),
                Quantity = 5
            };
        }

        private static Domain.Entities.ProductType GetValidDomainProductType5()
        {
            return new Domain.Entities.ProductType(new ProductTypeValidator())
            {
                Name = "cd",
                ProductTypeId = 2
            };
        }

        public static Domain.Entities.Product GetValidDomainProduct4Has0Quantity()
        {
            return new Domain.Entities.Product(new ProductValidator(), new ProductTypeValidator())
            {
                SKU = "F-S4d7PK",
                Name = "Poetry 4",
                Type = GetValidDomainProductType4(),
                Quantity = 0
            };
        }

        private static Domain.Entities.ProductType GetValidDomainProductType4()
        {
            return new Domain.Entities.ProductType(new ProductTypeValidator())
            {
                Name = "book",
                ProductTypeId = 1
            };
        }

        public static Domain.Entities.Product GetValidDomainProductWithDuplicateSkuWithProduct2AndDifferentNameAndType()
        {
            return new Domain.Entities.Product(new ProductValidator(), new ProductTypeValidator())
            {
                SKU = GetValidDomainProduct2().SKU,
                Name = "Training Material 5",
                Type = GetValidDomainProductType3(),
                Quantity = 5
            };
        }

        public static Domain.Entities.Product GetValidDomainProduct6()
        {
            return new Domain.Entities.Product(new ProductValidator(), new ProductTypeValidator())
            {
                SKU = "grr2707",
                Name = "A song of ice and fire",
                Type = GetValidDomainProductType6(),
                Quantity = 5
            };

        }

        private static Domain.Entities.ProductType GetValidDomainProductType6()
        {
            return new Domain.Entities.ProductType(new ProductTypeValidator())
            {
                Name = "boOk",
                ProductTypeId = 1
            };
        }
    }
}
