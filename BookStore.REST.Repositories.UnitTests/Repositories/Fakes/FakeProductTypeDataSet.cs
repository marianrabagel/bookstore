﻿using BookStore.REST.DataAccess.Entities;

namespace BookStore.REST.Repositories.UnitTests.Repositories.Fakes
{
    public class FakeProductTypeDataSet : FakeDbSet<ProductType>
    {
        
    }
}
