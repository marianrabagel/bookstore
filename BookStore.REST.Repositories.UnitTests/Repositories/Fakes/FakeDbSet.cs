﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace BookStore.REST.Repositories.UnitTests.Repositories.Fakes
{
    public abstract class FakeDbSet<TEntity> : DbSet<TEntity>, 
        IDbSet<TEntity>, IQueryable<TEntity>, IEnumerable<TEntity>, IEnumerable, IQueryable
        where TEntity : class
    {
        private readonly List<TEntity> data;

        protected FakeDbSet()
        {
            data = new List<TEntity>();
        }

        public override TEntity Add(TEntity item)
        {
            data.Add(item);
            return item;
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return data.GetEnumerator();
        }

        public Expression Expression { get; }
        public Type ElementType { get; }
        public IQueryProvider Provider { get; }
        public TEntity Find(params object[] keyValues)
        {
            throw new NotImplementedException();
        }

        public override TEntity Remove(TEntity entity)
        {
            return entity;
        }

        public override TEntity Attach(TEntity entity)
        {
            return entity;
        }

        public override TEntity Create()
        {
            throw new NotImplementedException();
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, TEntity
        {
            return base.Create<TDerivedEntity>();
        }

        public ObservableCollection<TEntity> Local { get; }
    }
}
