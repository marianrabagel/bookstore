﻿using System.Data.Entity;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.DataAccess.Entities;

namespace BookStore.REST.Repositories.UnitTests.Repositories.Fakes
{
    class FakeDataContext : IDataContext
    {
        public IDbSet<Product> Products { get; set; }
        public IDbSet<ProductType> ProductTypes { get; set; }

        public FakeDataContext()
        {
            ProductTypes = new FakeProductTypeDataSet();
            Products = new FakeProductDataSet();
        }

        public void Add(ProductType productType)
        {
            ProductTypes.Add(productType);
            SaveChanges();
        }

        public void Add(Product product)
        {
            Products.Add(product);
            SaveChanges();
        }

        public int SaveChanges()
        {
            return 0;
        }

        public void Clear()
        {
            ProductTypes = new FakeProductTypeDataSet();
            Products = new FakeProductDataSet();
        }
    }
}
