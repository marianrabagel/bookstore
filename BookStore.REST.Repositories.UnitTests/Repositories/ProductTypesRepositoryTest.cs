﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.Repositories.UnitTests.Repositories.DI;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets;
using BookStore.REST.Repository;
using BookStore.REST.Repository.Contracts;
using BookStore.REST.Repository.Contracts.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.UnitTests;

namespace BookStore.REST.Repositories.UnitTests.Repositories
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ProductTypesRepositoryTest : BaseTests<DependencyResolver>
    {
        private IProductTypesRepository productTypesRepository;
        private FakeDataContext dataContext;
        private IProductTypeMapper productTypeMapper;
        private AbstractListValidator<ProductTypesList, ProductType> productTypesListValidator;

        [TestInitialize]
        public void Setup()
        {
            dataContext = (FakeDataContext) DependencyResolver.CreateInstance<IDataContext>();
            productTypeMapper = DependencyResolver.CreateInstance<IProductTypeMapper>();
            productTypesRepository = DependencyResolver.CreateInstance<IProductTypesRepository>();
        }

        [TestMethod]
        public void TestThatFromAContextWith2ProductTypesGetProductTypesWillReturn2ProductTypes()
        {
            dataContext.Add(DataSets.GetValidDataAccessProductType1());
            dataContext.Add(DataSets.GetValidDataAccessProductType2());
            CreateProductTypesRepository();
            var list = productTypesRepository.GetProductTypes();
            
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(DataSets.GetValidDataAccessProductType1().Name, list[0].Name);
            Assert.AreEqual(DataSets.GetValidDataAccessProductType2().Name, list[1].Name);
        }

        [TestMethod]
        public void TestThatFromAnEmptyContextGetProductTypesWillReturnAnEmptyList()
        {
            CreateProductTypesRepository();
            dataContext.Clear();
            var list = productTypesRepository.GetProductTypes();

            Assert.AreEqual(0, list.Count);
        }

        [TestMethod]
        public void TestThatFromADomainProductTypesListTheProductTypesWillBeAddedToTheContext()
        {
            CreateProductTypesRepository();
            IEnumerable<ProductType> productTypes = new List<ProductType>()
            {
                DataSets.GetValidDomainProductType1(),
                DataSets.GetValidDomainProductType2()
            };

            var initialCount = productTypesRepository.GetProductTypes().Count;
            productTypesRepository.PersistProductTypes(productTypes);
            var actualCount = productTypesRepository.GetProductTypes().Count;

            Assert.AreEqual(initialCount + productTypes.Count(), actualCount);
        }

        private void CreateProductTypesRepository()
        {
            productTypesListValidator = new ProductTypesListValidator();
            productTypesRepository = new ProductTypesRepository(dataContext, productTypeMapper, productTypesListValidator);
        }

        [TestMethod]
        public void TestThatWhenAddingAndEmptyListTheContextWillNotChange()
        {
            CreateProductTypesRepository();
            var productTypes = new List<ProductType>();

            var initialCount = productTypesRepository.GetProductTypes().Count;
            productTypesRepository.PersistProductTypes(productTypes);
            var actualCount = productTypesRepository.GetProductTypes().Count;

            Assert.AreEqual(initialCount, actualCount);
        }
    }
}
