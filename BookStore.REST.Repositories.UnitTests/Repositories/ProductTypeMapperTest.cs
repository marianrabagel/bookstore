﻿using System.Diagnostics.CodeAnalysis;
using BookStore.REST.Repositories.UnitTests.Repositories.DI;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets;
using BookStore.REST.Repository.Contracts.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.UnitTests;

namespace BookStore.REST.Repositories.UnitTests.Repositories
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ProductTypeMapperTest : BaseTests<DependencyResolver>
    {
        private IProductTypeMapper productTypeMapper;

        [TestInitialize]
        public void Setup()
        {
            productTypeMapper = DependencyResolver.CreateInstance<IProductTypeMapper>();
        }

        [TestMethod]
        public void TestThatTheDataAccessProductTypeItsFilledWithDomainProductType()
        {
            var productType = DataSets.GetValidDomainProductType1();

            DataAccess.Entities.ProductType product = productTypeMapper.FillDataAccessProductTypeFromDomain(productType);

            Assert.AreEqual(product.Name, DataSets.GetValidDomainProductType1().Name);
        }

        [TestMethod]
        public void TestThatTheDomainProductTypeIsFilledFromDataAccessProductType()
        {
            DataAccess.Entities.ProductType productType = DataSets.GetValidDataAccessProductType1();

            Domain.Entities.ProductType product = productTypeMapper.FillDomainProductTypeFromDataAccess(productType);

            Assert.AreEqual(product.Name, productType.Name);
        }
    }
}
