﻿using System.Diagnostics.CodeAnalysis;
using Autofac;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes;
using BookStore.REST.Repository;
using BookStore.REST.Repository.Contracts;
using BookStore.REST.Repository.Contracts.Mappers;
using BookStore.REST.Repository.Mappers;
using FluentValidation;
using Services.Configuration.DI.Contracts;

namespace BookStore.REST.Repositories.UnitTests.Repositories.DI
{
    [ExcludeFromCodeCoverage]
    public class DependencyResolver : AbstractDependencyResolver
    {
        public override void PerformRegistrations()
        {
            ContainerBuilder.RegisterType<FakeDataContext>().As<IDataContext>();

            ContainerBuilder.RegisterType<ProductTypesRepository>().As<IProductTypesRepository>();
            ContainerBuilder.RegisterType<ProductTypesListValidator>().As<AbstractListValidator<ProductTypesList, ProductType>>();
            ContainerBuilder.RegisterType<ProductTypeValidator>().As<AbstractValidator<ProductType>>();
            ContainerBuilder.RegisterType<ProductTypeMapper>().As<IProductTypeMapper>();

            ContainerBuilder.RegisterType<ProductsRepository>().As<IProductsRepository>();
            ContainerBuilder.RegisterType<ProductsListValidator>().As<AbstractListValidator<ProductsList, Product>>();
            ContainerBuilder.RegisterType<ProductValidator>().As<AbstractValidator<Product>>();
            ContainerBuilder.RegisterType<ProductMapper>().As<IProductMapper>();
            
            base.PerformRegistrations();
        }
    }
}
