﻿using System.Diagnostics.CodeAnalysis;
using BookStore.REST.DataAccess.Entities;
using BookStore.REST.Repositories.UnitTests.Repositories.DI;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets;
using BookStore.REST.Repository.Contracts.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.UnitTests;

namespace BookStore.REST.Repositories.UnitTests.Repositories
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ProductMapperTest : BaseTests<DependencyResolver>
    {
        private IProductMapper productMapper;

        [TestInitialize]
        public void Setup()
        {
            productMapper = DependencyResolver.CreateInstance<IProductMapper>();
        }

        [TestMethod]
        public void TestThatDomainProductIsFilledFormADataAccessProduct()
        {
            Product product = DataSets.GetValidDataAccessProduct1();
            Domain.Entities.Product domainProduct = productMapper.FillDomainProductFromDataAccess(product);

            Assert.AreEqual(domainProduct.Name, product.Name);
            Assert.AreEqual(domainProduct.SKU, product.SKU);
            Assert.AreEqual(domainProduct.Quantity, product.Quantity);
        }

        [TestMethod]
        public void TestThatDataAccessProductIsFilledFromADomainProduct()
        {
            Domain.Entities.Product product = DataSets.GetValidDomainProduct1();

            var dataAccessProduct = productMapper.FillDataAccessProductFromDomain(product);

            Assert.AreEqual(product.Name, dataAccessProduct.Name);
            Assert.AreEqual(product.SKU, dataAccessProduct.SKU);
            Assert.AreEqual(product.Quantity, dataAccessProduct.Quantity);
        }
    }
}
