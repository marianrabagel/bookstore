﻿using System.Diagnostics.CodeAnalysis;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.Repositories.UnitTests.Repositories.DI;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets;
using BookStore.REST.Repository;
using BookStore.REST.Repository.Contracts;
using BookStore.REST.Repository.Contracts.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.UnitTests;

namespace BookStore.REST.Repositories.UnitTests.Repositories
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ProductsRepositoryTest : BaseTests<DependencyResolver>
    {
        private FakeDataContext fakeDataContext;
        private IProductsRepository productsRepository;
        private IProductTypesRepository productTypesRepository;
        private AbstractListValidator<ProductsList, Product> productListValidator;
        private AbstractListValidator<ProductTypesList, ProductType> productTypeListValidator;
        private IProductMapper productMapper;
        private IProductTypeMapper productTypeMapper;

        [TestInitialize]
        public void Setup()
        {
            fakeDataContext = (FakeDataContext) DependencyResolver.CreateInstance<IDataContext>();
            productsRepository = DependencyResolver.CreateInstance<IProductsRepository>();
            productTypesRepository = DependencyResolver.CreateInstance<IProductTypesRepository>();
            productMapper = DependencyResolver.CreateInstance<IProductMapper>();
            productTypeMapper = DependencyResolver.CreateInstance<IProductTypeMapper>();
        }

        [TestMethod]
        public void TestThatFromAContextWith2ProductTypesGetProductsWithThoseProductTypes()
        {
            fakeDataContext.Add(DataSets.GetValidDataAccessProduct1());
            fakeDataContext.Add(DataSets.GetValidDataAccessProduct2());
            fakeDataContext.Add(DataSets.GetValidDataAccessProductType1());
            fakeDataContext.Add(DataSets.GetValidDataAccessProductType2());

            CreateProductRepository();
            
            var list = productsRepository.GetProducts();
            
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(DataSets.GetValidDataAccessProduct1().SKU, list[0].SKU);
            Assert.AreEqual(DataSets.GetValidDataAccessProduct1().Name, list[0].Name);
            Assert.AreEqual(DataSets.GetValidDataAccessProduct1().Quantity, list[0].Quantity);
            //Assert.AreEqual(DataSets.GetValidDataAccessProduct1().Type.Name, list[0].Type.Name);

            Assert.AreEqual(DataSets.GetValidDataAccessProduct2().SKU, list[1].SKU);
            Assert.AreEqual(DataSets.GetValidDataAccessProduct2().Name, list[1].Name);
            Assert.AreEqual(DataSets.GetValidDataAccessProduct2().Quantity, list[1].Quantity);
            //Assert.AreEqual(DataSets.GetValidDataAccessProduct2().Type.Name, list[1].Type.Name);
        }

        [TestMethod]
        public void TestThatGetProductsFromAnEmptyDataContextReturnsAnEmptyList()
        {
            CreateProductRepository();
            var list = productsRepository.GetProducts();

            Assert.AreEqual(0, list.Count);
        }
        
        [TestMethod]
        public void TestThatFromAddingAnEmptyProductListWillNotChangeTheContext()
        {
            CreateProductRepository();
            ProductsList productList = new ProductsList(new ProductsListValidator());
            var initialCount = productsRepository.GetProducts().Count;
            productsRepository.PersistProductsImportData(productList);
            int count = productsRepository.GetProducts().Count;

            Assert.AreEqual(initialCount, count);
        }

        [TestMethod]
        public void TestThatAddingAListOfProductsWillPersistToTheContext()
        {
            CreateProductRepository();
            var initalCount = productsRepository.GetProducts().Count;
            ProductsList productList = new ProductsList(productListValidator)
            {
                DataSets.GetValidDomainProduct1(),
                DataSets.GetValidDomainProduct2()
            };
            productsRepository.PersistProductsImportData(productList);
            var count = productsRepository.GetProducts().Count;

            Assert.AreEqual(initalCount + productList.Count, count);
        }
        
        private void CreateProductRepository()
        {
            productListValidator = new ProductsListValidator();
            productTypeListValidator = new ProductTypesListValidator();
            productTypesRepository = new ProductTypesRepository(fakeDataContext, productTypeMapper, productTypeListValidator);
            productsRepository = new ProductsRepository(fakeDataContext, productMapper, productTypesRepository, productListValidator);
        }
    }
}
