﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.BusinessLogic;
using BookStore.REST.BusinessLogic.Contracts.Facades;
using BookStore.REST.Controllers;
using BookStore.REST.Repositories.UnitTests.BussinessLogic.Fakes;
using BookStore.REST.Repositories.UnitTests.Controllers.DI;
using BookStore.REST.Repositories.UnitTests.Repositories.Fakes.DbSets;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Services.JsonConverter;
using Services.JsonConverter.Contracts.Facades;
using Services.JsonConverter.Facades;
using Services.UnitTests;

namespace BookStore.REST.Repositories.UnitTests.Controllers
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ProductsControllerTests : BaseTests<DependencyResolver>
    {
        private ProductsController productsController;
        private IProductsImporterFacade productsImporterFacade;
        private JArray jsonProductsArray;

        [TestInitialize]
        public void Setup()
        {
            productsImporterFacade = DependencyResolver.CreateInstance<IProductsImporterFacade>();
            jsonProductsArray = new JArray();
        }

        [TestMethod]
        public void TestThatImportingFromAnEmptyJarrayThrowsException()
        {
            CreateProductsImporterController();
            var response = productsController.Save(jsonProductsArray);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.InternalServerError);
        }

        [TestMethod]
        public void TestThatImportingFromANullJarrayThrowsException()
        {
            CreateProductsImporterController();
            var response = productsController.Save(null);

            Assert.IsTrue(response.StatusCode == HttpStatusCode.InternalServerError);
        }

        [TestMethod]
        public void TestThatImporting2ValidProductsWillSaveThoseProductsAndTypes()
        {
            CreateProductsImporterController();
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct1());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct2());
            var response = productsController.Save(jsonProductsArray);
            
            Assert.IsTrue(response.StatusCode == HttpStatusCode.Created);
        }

        [TestMethod]
        public void TestThatImportingAProductsListWithADuplicateSkuAndDifferentNameAndTypeWillNotifyUser()
        {
            CreateProductsImporterController();
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProduct1());
            AddJtokenToJsonProductsArray(DataSets.GetValidDomainProductWithDuplicateSkuWithProduct2AndDifferentNameAndType());
            var response = productsController.Save(jsonProductsArray);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.Created);
            string notificationMessage = "There is a product with the same SKU and different Name and Type";
            Assert.IsTrue(response.ReasonPhrase == notificationMessage);
        }
        
        private void CreateProductsImporterController()
        {
            productsImporterFacade.ProductsRepository = new FakeProductRepository(DataSets.GetValidDomainProduct2());
            productsImporterFacade.ProductTypesRepository = new FakeProductTypeRepository();

            IProductValidationFacade productValidationFacade = new ProductValidationFacade()
            {
                ProductValidator = new ProductValidator(),
                ProductTypeValidator = new ProductTypeValidator(),
                ProductsListValidator = new ProductsListValidator()
            };

            productsImporterFacade.JsonToProductsConverter = new JsonToProductsConverter(productValidationFacade);
            var productsImporter = new ProductsImporter(productsImporterFacade);
            productsController = new ProductsController(productsImporter);
        }

        private void AddJtokenToJsonProductsArray(Product domainProduct)
        {
            var product = JsonConvert.SerializeObject(domainProduct);
            JToken jtoken1 = JToken.Parse(product);
            jsonProductsArray.Add(jtoken1);
        }
    }
}
