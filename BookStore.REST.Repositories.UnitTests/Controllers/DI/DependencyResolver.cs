﻿using Autofac;
using BookStore.REST.BusinessLogic.Contracts.Facades;
using BookStore.REST.BusinessLogic.Facades;
using Services.Configuration.DI.Contracts;

namespace BookStore.REST.Repositories.UnitTests.Controllers.DI
{
    public class DependencyResolver : AbstractDependencyResolver
    {
        public override void PerformRegistrations()
        {
            ContainerBuilder.RegisterType<ProductsImporterFacade>().As<IProductsImporterFacade>();

            base.PerformRegistrations();
        }
    }
}
