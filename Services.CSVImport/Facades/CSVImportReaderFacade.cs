﻿using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using FluentValidation;
using Services.Configuration.Contracts;
using Services.CSVImport.Contracts;
using Services.CSVImport.Contracts.Facades;

namespace Services.CSVImport.Facades
{
    public class CSVImportReaderFacade: ICSVImportReaderFacade
    {
        public ICSVImportProvider CSVImportProvider { get; set; }
        public IConfigurationReader ConfigurationReader { get; set; }
        public AbstractListValidator<ProductsList, Product> ProductsListValidator { get; set; }
        public AbstractValidator<Product> ProductValidator { get; set; }
        public AbstractValidator<ProductType> ProductTypeValidator { get; set; }
    }
}
