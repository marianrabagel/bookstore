﻿using BookStore.Domain.Entities;

namespace Services.CSVImport.Contracts
{
    public interface ICSVImportReader
    {
        ProductsList GetProductsListFromCSV();
    }
}
