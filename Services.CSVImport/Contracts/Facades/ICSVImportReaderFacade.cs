﻿using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using FluentValidation;
using Services.Configuration.Contracts;

namespace Services.CSVImport.Contracts.Facades
{
    public interface ICSVImportReaderFacade
    {
        ICSVImportProvider CSVImportProvider { get; set; }
        IConfigurationReader ConfigurationReader { get; set; }
        AbstractListValidator<ProductsList, Product> ProductsListValidator { get; set; }
        AbstractValidator<Product> ProductValidator { get; set; }
        AbstractValidator<ProductType> ProductTypeValidator { get; set; }
    }
}
