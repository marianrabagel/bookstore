﻿using System.Collections.Generic;

namespace Services.CSVImport.Contracts
{
    public interface ICSVImportProvider
    {
        IEnumerable<string> ReadDataFileContent();
    }
}
