﻿using Services.CSVImport.Contracts;
using System.Collections.Generic;
using System.IO;

namespace Services.CSVImport
{
    public class CSVImportProvider : ICSVImportProvider
    {
        private readonly Stream dataStream;

        public CSVImportProvider(Stream dataStream)
        {
            this.dataStream = dataStream;
        }

        public IEnumerable<string> ReadDataFileContent()
        {
            using (dataStream)
            {
                dataStream.Position = 0;

                using (var streamReader = new StreamReader(dataStream))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        yield return line;
                    }

                    streamReader.Close();
                }

                dataStream.Close();
            }
        }
    }
}
