﻿using System;
using System.Collections.Generic;
using BookStore.Domain.Entities;
using Services.CSVImport.Contracts;
using Services.CSVImport.Contracts.Facades;

namespace Services.CSVImport
{
    public class CSVImportReader : ICSVImportReader
    {
        private const string ErrInvalidCSVFile = "Invalid CSV file";

        private readonly ICSVImportReaderFacade csvImportReaderFacade;

        public CSVImportReader(ICSVImportReaderFacade csvImportReaderFacade)
        {
            this.csvImportReaderFacade = csvImportReaderFacade;
        }

        public ProductsList GetProductsListFromCSV()
        {
            var productsList = new ProductsList(csvImportReaderFacade.ProductsListValidator);
            Dictionary<int, string> tokenIndexMapping = null;

            var lineCount = 0;
            foreach (var line in csvImportReaderFacade.CSVImportProvider.ReadDataFileContent())
            {
                if (lineCount == 0)
                {
                    tokenIndexMapping = BuildColumnsMapping(line);
                }
                else
                {
                    productsList.Add(FillProductWithCSVData(line, tokenIndexMapping));
                }

                lineCount++;
            }

            return productsList;
        }

        private Product FillProductWithCSVData(string line, IReadOnlyDictionary<int, string> tokenIndexMapping)
        {
            var product = new Product(csvImportReaderFacade.ProductValidator, csvImportReaderFacade.ProductTypeValidator);

            var columns = GetCSVColumns(line);
            for (var i = 0; i < columns.Length; i++)
            {
                var columnName = tokenIndexMapping[i];
                var columnValue = columns[i];

                switch (columnName)
                {
                    case "SKU":
                        product.SKU = columnValue;
                        break;
                    case "Product Name":
                        product.Name = columnValue;
                        break;
                    case "Product Type":
                        product.Type.Name = columnValue;
                        break;
                    case "Quantity":
                        product.Quantity = Convert.ToInt32(columnValue);
                        break;
                }
            }

            return product;
        }

        private Dictionary<int, string> BuildColumnsMapping(string line)
        {
            var columns = GetCSVColumns(line);

            var tokenIndexMapping = new Dictionary<int, string>();
            for (var i = 0; i < columns.Length; i++)
            {
                tokenIndexMapping.Add(i, columns[i]);
            }

            if (tokenIndexMapping.Count == 0)
            {
                throw new InvalidOperationException(ErrInvalidCSVFile);
            }

            return tokenIndexMapping;
        }

        private string[] GetCSVColumns(string line)
        {
            var columns = line.Split(new[] { GetDataSeparator() }, StringSplitOptions.None);
            if (columns.Length <= 1)
            {
                throw new InvalidOperationException(ErrInvalidCSVFile);
            }
            return columns;
        }

        private string GetDataSeparator()
        {
            return csvImportReaderFacade.ConfigurationReader.GetValueByKeyName<string>("DataSeparator");
        }
    }
}
