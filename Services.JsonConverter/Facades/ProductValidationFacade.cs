﻿using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using FluentValidation;
using Services.JsonConverter.Contracts.Facades;

namespace Services.JsonConverter.Facades
{
    public class ProductValidationFacade : IProductValidationFacade
    {
        public AbstractListValidator<ProductsList, Product> ProductsListValidator { get; set; }
        public AbstractValidator<Product> ProductValidator { get; set; }
        public AbstractValidator<ProductType> ProductTypeValidator { get; set; }
    }
}
