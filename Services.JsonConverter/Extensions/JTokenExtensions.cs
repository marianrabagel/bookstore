﻿using System;
using System.Globalization;
using Newtonsoft.Json.Linq;

namespace Services.JsonConverter.Extensions
{
    public static class JTokenExtensions
    {
        public static TResult GetPropertyValueByName<TResult>(this JToken token, string propertyName)
        {
            if (token == null || token[propertyName] == null)
            {
                return default(TResult);
            }

            try
            {
                return token[propertyName].Value<TResult>();
            }
            catch (FormatException ex)
            {
                var errMsg = string.Format(CultureInfo.InvariantCulture, "Invalid value for {0}", propertyName);
                throw new InvalidOperationException(errMsg, ex);
            }
        }
    }
}
