﻿using System;
using BookStore.Domain.Entities;
using Newtonsoft.Json.Linq;
using Services.JsonConverter.Contracts;
using Services.JsonConverter.Contracts.Facades;
using Services.JsonConverter.Extensions;

namespace Services.JsonConverter
{
    public class JsonToProductsConverter: IJsonToProductsConverter
    {
        private readonly IProductValidationFacade productValidationFacade;

        public JsonToProductsConverter(IProductValidationFacade productValidationFacade)
        {
            this.productValidationFacade = productValidationFacade;
        }

        public ProductsList GetProductsListFromJson(JArray jsonProductsArray)
        {
            if (jsonProductsArray == null)
            {
                throw new ArgumentNullException(nameof(jsonProductsArray));
            }
            
            var productsList = new ProductsList(productValidationFacade.ProductsListValidator);
            
            foreach (var jsonProduct in jsonProductsArray)
            {
                productsList.Add(FillProductWithJsonData(jsonProduct));
            }

            return productsList;
        }

        private Product FillProductWithJsonData(JToken jsonProduct)
        {
            return new Product(productValidationFacade.ProductValidator, productValidationFacade.ProductTypeValidator)
            {
                SKU = jsonProduct.GetPropertyValueByName<string>("SKU"),
                Name = jsonProduct.GetPropertyValueByName<string>("Name"),
                Type = FillProductTypeWithJsonData(jsonProduct["Type"]),
                Quantity = jsonProduct.GetPropertyValueByName<int>("Quantity"),
            };
        }

        private ProductType FillProductTypeWithJsonData(JToken jsonType)
        {
            return new ProductType(productValidationFacade.ProductTypeValidator)
            {
                Name = jsonType.GetPropertyValueByName<string>("Name"),
            };
        }
    }
}
