﻿using BookStore.Domain.Entities;
using Newtonsoft.Json.Linq;

namespace Services.JsonConverter.Contracts
{
    public interface IJsonToProductsConverter
    {
        ProductsList GetProductsListFromJson(JArray jsonProductsArray);
    }
}