﻿using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using FluentValidation;

namespace Services.JsonConverter.Contracts.Facades
{
    public interface IProductValidationFacade
    {
        AbstractListValidator<ProductsList, Product> ProductsListValidator { get; set; }
        AbstractValidator<ProductType> ProductTypeValidator { get; set; }
        AbstractValidator<Product> ProductValidator { get; set; }
    }
}