﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics.CodeAnalysis;
using BookStore.REST.DataAccess.Contracts;
using BookStore.REST.DataAccess.Entities;

namespace BookStore.REST.DataAccess
{
    [ExcludeFromCodeCoverage]
    public class DataContext : DbContext, IDataContext
    {
        public DataContext(string connectionStringName)
            : base(connectionStringName)
        {
            Database.SetInitializer<DataContext>(new CreateDatabaseIfNotExists<DataContext>());
        }

        public IDbSet<Product> Products { get; set; }
        public IDbSet<ProductType> ProductTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Product>().ToTable("Products", "dbo");
            modelBuilder.Entity<ProductType>().ToTable("ProductTypes", "dbo");
        }
    }
}
