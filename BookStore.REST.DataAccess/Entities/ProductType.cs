﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.REST.DataAccess.Entities
{
    public class ProductType
    {
        [Key]
        public int ProductTypeId { get; set; }
        public string Name { get; set; }
    }
}