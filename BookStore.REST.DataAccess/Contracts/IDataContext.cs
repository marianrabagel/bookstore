﻿using System.Data.Entity;
using BookStore.REST.DataAccess.Entities;

namespace BookStore.REST.DataAccess.Contracts
{
    public interface IDataContext
    {
        IDbSet<Product> Products { get; set; }
        IDbSet<ProductType> ProductTypes { get; set; }

        int SaveChanges();
    }
}
