﻿using System.Diagnostics.CodeAnalysis;
using BookStore.Console.Contracts.Facades;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using log4net;
using Services.Configuration.Contracts;
using Services.CSVImport.Contracts;
using Services.Rest.Contracts.Factories;

namespace BookStore.Console.Facades
{
    [ExcludeFromCodeCoverage]
    public class ExecutionContextFacade : IExecutionContextFacade
    {
        public ILog Log { get; set; }
        public ICSVImportReader CSVImportReader { get; set; }
        public AbstractListValidator<ProductsList, Product> ProductsListValidator { get; set; }
        public IConfigurationReader ConfigurationReader { get; set; }
        public RestRequestFactory RestRequestFactory { get; set; }
    }
}
