﻿using BookStore.Console.Contracts.Facades;
using BookStore.Console.Controllers;
using BookStore.Console.Facades;
using BookStore.DI;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using log4net;
using Services.Configuration.Contracts;
using Services.CSVImport.Contracts;
using Services.Rest.Contracts.Factories;

namespace BookStore.Console
{
    public class Program
    {
        public static void Main()
        {
            var importController = new ImportProductsController(CreateExecutionContext());
            importController.DoWork();

            System.Console.WriteLine("Press any key to continue.");
            System.Console.ReadLine();
        }

        private static IExecutionContextFacade CreateExecutionContext()
        {
            var dependencyResolver = GetDependencyResolver();

            return new ExecutionContextFacade
            {
                CSVImportReader = dependencyResolver.CreateInstance<ICSVImportReader>(),
                Log = dependencyResolver.CreateInstance<ILog>(),
                ConfigurationReader = dependencyResolver.CreateInstance<IConfigurationReader>(),
                ProductsListValidator = dependencyResolver.CreateInstance<AbstractListValidator<ProductsList, Product>>(),
                RestRequestFactory = dependencyResolver.CreateInstance<RestRequestFactory>()
            };
        }

        private static DependencyResolver GetDependencyResolver()
        {
            var dependencyResolver = new DependencyResolver();
            dependencyResolver.PerformRegistrations();
            return dependencyResolver;
        }
    }
}
