﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using BookStore.Console.Contracts.Facades;
using BookStore.Domain.Entities;
using Services.Rest.Contracts.Factories;
using Services.Rest.Factories;

namespace BookStore.Console.Controllers
{
    [ExcludeFromCodeCoverage]
    public class ImportProductsController
    {
        private readonly IExecutionContextFacade executionContext;

        public ImportProductsController(IExecutionContextFacade executionContext)
        {
            this.executionContext = executionContext;
        }

        public void DoWork()
        {
            executionContext.Log.Info("Starting data import...");

            var startTime = DateTime.Now;

            try
            {
                var products = executionContext.CSVImportReader.GetProductsListFromCSV();
                products.ValidateAndThrow();
                ExecuteRequest(products);
            }
            catch (Exception ex)
            {
                executionContext.Log.Error("Error ocurred: " + ex.Message);
                return;
            }

            var totalTimeInSeconds = Math.Ceiling((DateTime.Now - startTime).TotalSeconds);

            executionContext.Log.Info("Data import done. " +
                string.Format(CultureInfo.InvariantCulture, "Total time: {0} seconds. ", totalTimeInSeconds));
        }

        private void ExecuteRequest(ProductsList products)
        {
            var url = string.Format(CultureInfo.InvariantCulture, "{0}{1}",
                executionContext.ConfigurationReader.GetValueByKeyName<string>("BaseAddress"),
                executionContext.ConfigurationReader.GetValueByKeyName<string>("EndPoint"));

            RestRequestFactory restRequestFactory = new AnonymousAuthenticatedRestRequestFactory();

            var anonymousAuthenticatedRESTRequest = restRequestFactory.CreateSaveRequest(new Uri(url), products);

            anonymousAuthenticatedRESTRequest.InvokeAsync().ConfigureAwait(false);
        }
    }
}