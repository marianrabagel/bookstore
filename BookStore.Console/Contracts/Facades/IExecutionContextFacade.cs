﻿using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using log4net;
using Services.Configuration.Contracts;
using Services.CSVImport.Contracts;

namespace BookStore.Console.Contracts.Facades
{
    public interface IExecutionContextFacade
    {
        ILog Log { get; set; }
        ICSVImportReader CSVImportReader { get; set; }
        AbstractListValidator<ProductsList, Product> ProductsListValidator { get; set; }
        IConfigurationReader ConfigurationReader { get; set; }
    }
}