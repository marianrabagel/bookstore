﻿using Autofac;

namespace Services.Configuration.DI.Contracts
{
    public abstract class AbstractDependencyResolver
    {
        protected AbstractDependencyResolver()
        {
            ContainerBuilder = new ContainerBuilder();
        }

        public virtual void PerformRegistrations()
        {
            Container = ContainerBuilder.Build();
        }

        public T CreateInstance<T>()
        {
            using (Container.BeginLifetimeScope())
            {
                return Container.Resolve<T>();
            }
        }

        protected ContainerBuilder ContainerBuilder { get; }

        public IContainer Container { get; private set; }
    }
}
