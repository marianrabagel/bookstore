﻿using System;
using Services.Configuration.Contracts;

namespace Services.Configuration
{
    public class ConfigurationReader : IConfigurationReader
    {
        private readonly IConfigurationProvider configurationProvider;

        public ConfigurationReader(IConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }

        public TResult GetValueByKeyName<TResult>(string keyName)
        {
            if (string.IsNullOrWhiteSpace(keyName))
            {
                throw new ArgumentNullException(nameof(keyName));
            }

            var settingValue = configurationProvider.GetApplicationSettings(keyName) ??
                configurationProvider.GetConnectionStrings(keyName);

            if (settingValue != null)
            {
                return (TResult)Convert.ChangeType(settingValue, typeof(TResult));
            }

            throw new InvalidOperationException(string.Format(Strings.Strings.ErrKeyNotFoundMessage, keyName));
        }
    }
}