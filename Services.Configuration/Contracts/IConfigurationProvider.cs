﻿namespace Services.Configuration.Contracts
{
    public interface IConfigurationProvider
    {
        string GetApplicationSettings(string settingName);
        string GetConnectionStrings(string settingName);
    }
}
