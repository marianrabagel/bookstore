﻿namespace Services.Configuration.Contracts
{
    public interface IConfigurationReader
    {
        TResult GetValueByKeyName<TResult>(string keyName);
    }
}
