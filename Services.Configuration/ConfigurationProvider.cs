﻿using Services.Configuration.Contracts;
using System.Configuration;

namespace Services.Configuration
{
    public class ConfigurationProvider : IConfigurationProvider
    {
        public string GetApplicationSettings(string settingName)
        {
            return ConfigurationManager.AppSettings[settingName];
        }

        public string GetConnectionStrings(string settingName)
        {
            var settingValue = ConfigurationManager.ConnectionStrings[settingName];
            return settingValue != null ?
                settingValue.ConnectionString :
                null;
        }
    }
}
