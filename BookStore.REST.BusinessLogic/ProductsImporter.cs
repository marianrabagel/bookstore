﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using BookStore.REST.BusinessLogic.Comparer;
using BookStore.REST.BusinessLogic.Contracts;
using BookStore.REST.BusinessLogic.Contracts.Facades;
using Newtonsoft.Json.Linq;

namespace BookStore.REST.BusinessLogic
{
    public class ProductsImporter : IProductsImporter
    {
        private readonly IProductsImporterFacade productsImporterFacade;
        public string ErrorMessage { get; } = "There is a product with the same SKU and different Name and Type";

        public ProductsImporter(IProductsImporterFacade productsImporterFacade)
        {
            this.productsImporterFacade = productsImporterFacade;
        }

        public string DoWork(JArray jsonProductsArray)
        {
            string message;

            var productsList = GetValidProductsForImport(jsonProductsArray);
            var filteredProducts = ApplyExcludeQuantityGreaterThanZeroBusinessRule(productsList);
            message = ApplyExcludeProductsWithSameSkuAndDifferentNameAndTypeBusinessRule(filteredProducts);
            HandleProductTypesImport(filteredProducts);
            UpdateProductTypeIdIn(filteredProducts);
            //get productTypeId
            HandleProductsImport(filteredProducts);

            return message;
        }

        private void UpdateProductTypeIdIn(ProductsList filteredProducts)
        {
            var productTypesList = productsImporterFacade.ProductTypesRepository.GetProductTypes();

            foreach (var product in filteredProducts)
            {
                product.Type.ProductTypeId = productTypesList.First(x => x.Name.Equals(product.Type.Name,
                        StringComparison.InvariantCultureIgnoreCase)).ProductTypeId;
            }
        }

        private ProductsList GetValidProductsForImport(JArray jsonProductsArray)
        {
            var productsList = productsImporterFacade.JsonToProductsConverter.GetProductsListFromJson(jsonProductsArray);
            productsList.ValidateAndThrow();
            return productsList;
        }

        private ProductsList ApplyExcludeQuantityGreaterThanZeroBusinessRule(ProductsList productsList)
        {
            ProductsList list = new ProductsList(new ProductsListValidator());
            foreach (var x in productsList)
            {
                if (x.Quantity > 0)
                    list.Add(x);
            }
            return list;
        }
        private void HandleProductTypesImport(ProductsList productsList)
        {
            var productTypesList = productsImporterFacade.ProductTypesRepository.GetProductTypes();

            var productTypesForImport = new List<ProductType>();

            ProductTypeComparer productTypeComparer = new ProductTypeComparer();
            var distinctProductTypes = productsList.Select(x => x.Type).Distinct(productTypeComparer);
            foreach (var productType in distinctProductTypes)
            {
                if (!productTypesForImport.Any(x => x.Name.Equals(productType.Name, StringComparison.InvariantCultureIgnoreCase)))
                {
                    productTypesForImport.Add(productType);
                }
            }

            var productTypesToBeAdded = new List<ProductType>();

            foreach (var productType in productTypesForImport)
            {
                if (!productTypesList.Any(
                    x => x.Name.Equals(productType.Name, StringComparison.InvariantCultureIgnoreCase)))
                {
                    productTypesToBeAdded.Add(productType);
                }
            }
            
            productsImporterFacade.ProductTypesRepository.PersistProductTypes(productTypesToBeAdded);
        }

        private void HandleProductsImport(ProductsList filteredProducts)
        {
            var dbProductsList = productsImporterFacade.ProductsRepository.GetProducts();
            var productsToAdd = GetInexistentProductsIn(dbProductsList, filteredProducts);
            AddProductsTo(dbProductsList, productsToAdd);
            ProductsList productsToUpdate = RemoveProductsFrom(filteredProducts, productsToAdd);
            UpdateProductsInto(dbProductsList, productsToUpdate);
            productsImporterFacade.ProductsRepository.PersistProductsImportData(dbProductsList);
        }

        private string ApplyExcludeProductsWithSameSkuAndDifferentNameAndTypeBusinessRule(ProductsList filteredProducts)
        {
            var dbProductsList = productsImporterFacade.ProductsRepository.GetProducts();
            ProductsList productsListToDrop = new ProductsList(new ProductsListValidator());

            foreach (var product in filteredProducts)
            {
                if (dbProductsList.Any(x => x.SKU == product.SKU
                                            && !x.Name.Equals(product.Name)
                                            && !x.Type.Name.Equals(product.Type.Name)))
                {
                    productsListToDrop.Add(product);
                }
            }
            RemoveProductsFrom(filteredProducts, productsListToDrop);

            if (productsListToDrop.Count > 0)
            {
                return ErrorMessage;
            }
            return "";
        }

        /// <summary>
        /// Be careful that the items are removed from the instance
        /// </summary>
        private ProductsList RemoveProductsFrom(ProductsList products, ProductsList productsToRemove)
        {
            foreach (var product in productsToRemove)
            {
                products.Remove(product);
            }

            return products;
        }
        
        private ProductsList GetInexistentProductsIn(ProductsList dbProductsList, ProductsList products)
        {
            ProductsList productsInexistentInList = new ProductsList(new ProductsListValidator());

            foreach (var product in products)
            {
                bool anyProductHasTheSameSkuNameAndType = dbProductsList
                    .Any(x => x.SKU.Equals(product.SKU, StringComparison.InvariantCultureIgnoreCase)
                              && x.Name.Equals(product.Name, StringComparison.InvariantCultureIgnoreCase)
                              && x.Type.Name.Equals(product.Type.Name, StringComparison.InvariantCultureIgnoreCase));

                if (!anyProductHasTheSameSkuNameAndType)
                {
                    productsInexistentInList.Add(product);
                }
            }
            return productsInexistentInList;
        }

        private static void AddProductsTo(ProductsList dbProductsList, ProductsList productsToAdd)
        {
            foreach (var product in productsToAdd)
            {
                dbProductsList.Add(product);
            }
        }

        private void UpdateProductsInto(ProductsList dbProductsList, ProductsList productsToUpdate)
        {
            foreach (var product in productsToUpdate)
            {
                dbProductsList
                    .Where(x => x.SKU.Equals(product.SKU, StringComparison.InvariantCultureIgnoreCase)
                                && x.Name.Equals(product.Name, StringComparison.InvariantCultureIgnoreCase)
                                && x.Type.Name.Equals(product.Type.Name,
                                    StringComparison.InvariantCultureIgnoreCase))
                    .ToList()
                    .ForEach(x => x.Quantity = x.Quantity + product.Quantity);
            }
        }
    }

}
