﻿using Newtonsoft.Json.Linq;

namespace BookStore.REST.BusinessLogic.Contracts
{
    public interface IProductsImporter
    {
        string DoWork(JArray jsonProductsArray);
    }
}