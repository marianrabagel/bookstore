﻿using BookStore.REST.Repository.Contracts;
using Services.JsonConverter.Contracts;

namespace BookStore.REST.BusinessLogic.Contracts.Facades
{
    public interface IProductsImporterFacade
    {
        IJsonToProductsConverter JsonToProductsConverter { get; set; }
        IProductsRepository ProductsRepository { get; set; }
        IProductTypesRepository ProductTypesRepository { get; set; }
    }
}