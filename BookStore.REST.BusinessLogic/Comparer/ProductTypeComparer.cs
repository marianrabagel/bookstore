﻿
using System;
using System.Collections.Generic;
using BookStore.Domain.Entities;

namespace BookStore.REST.BusinessLogic.Comparer
{
    public class ProductTypeComparer : IEqualityComparer<ProductType>
    {
        public bool Equals(ProductType productType1, ProductType productType2)
        {
            return productType1.Name.Equals(productType2.Name, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(ProductType productType)
        {
            return productType.GetHashCode();
        }
    }
}
