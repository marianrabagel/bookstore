﻿using BookStore.REST.BusinessLogic.Contracts.Facades;
using BookStore.REST.Repository.Contracts;
using Services.JsonConverter.Contracts;

namespace BookStore.REST.BusinessLogic.Facades
{
    public class ProductsImporterFacade : IProductsImporterFacade
    {
        public IJsonToProductsConverter JsonToProductsConverter { get; set; }
        public IProductsRepository ProductsRepository { get; set; }
        public IProductTypesRepository ProductTypesRepository { get; set; }
    }
}
