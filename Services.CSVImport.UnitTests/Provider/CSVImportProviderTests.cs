﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.CSVImport.Contracts;
using Services.CSVImport.UnitTests.Provider.DI;
using Services.UnitTests;

namespace Services.CSVImport.UnitTests.Provider
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class CSVImportProviderTests : BaseTests<DependencyResolver>
    {
        private ICSVImportProvider importProvider;
        private IEnumerable<string> content;

       [TestInitialize]
        public void SetUp()
        {
            importProvider = DependencyResolver.CreateInstance<ICSVImportProvider>();

            content = importProvider.ReadDataFileContent();
        }

        [TestMethod]
        public void TestThatTheCSVDataIsSuccessfullyRetrieved()
        {
            Assert.IsTrue(content.Contains("SKU;Product Name;Product Type;Quantity"));
        }

        [TestMethod]
        public void TestThatFromTheCSVDataAreRetrieved6LinesIncludingTheHeader()
        {
            Assert.AreEqual(6, content.Count());
        }
    }
}
