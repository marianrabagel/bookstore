using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace Services.CSVImport.UnitTests.Provider.Fakes
{
    [ExcludeFromCodeCoverage]
    public class FakeDataStream : Stream
    {
        private readonly MemoryStream memoryStream;

        private const string CSVContent =
@"SKU;Product Name;Product Type;Quantity
F-M7ncpy;Fairytale 1;Book;23
F-4JshmZ;Album 2;CD;2
F-qdS7HZ;Novel 3;BOOK;14
F-S4d7PK;Poetry 4;book;0
F-8nQw53;Training Material 5;cd;5";

        public FakeDataStream()
        {
            memoryStream = new MemoryStream();

            FillMemoryStreamWithTestData();
        }

        private void FillMemoryStreamWithTestData()
        {
            var stringBytes = System.Text.Encoding.UTF8.GetBytes(CSVContent);

            memoryStream.Write(stringBytes, 0, stringBytes.Length);
            memoryStream.Seek(0, SeekOrigin.Begin);
        }

        public override void Flush()
        {
            memoryStream.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return memoryStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            memoryStream.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return memoryStream.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            memoryStream.Write(buffer, offset, count);
        }

        public override bool CanRead => memoryStream.CanRead;
        public override bool CanSeek => memoryStream.CanSeek;
        public override bool CanWrite => memoryStream.CanWrite;
        public override long Length => memoryStream.Length;
        public override long Position
        {
            get => memoryStream.Position;
            set => memoryStream.Position = value;
        }
    }
}