﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using Autofac;
using Services.Configuration.DI.Contracts;
using Services.CSVImport.Contracts;
using Services.CSVImport.UnitTests.Provider.Fakes;

namespace Services.CSVImport.UnitTests.Provider.DI
{
    [ExcludeFromCodeCoverage]
    public class DependencyResolver : AbstractDependencyResolver
    {
        public override void PerformRegistrations()
        {
            ContainerBuilder.RegisterType<FakeDataStream>().As<Stream>();
            ContainerBuilder.RegisterType<CSVImportProvider>().As<ICSVImportProvider>();

            base.PerformRegistrations();
        }
    }
}
