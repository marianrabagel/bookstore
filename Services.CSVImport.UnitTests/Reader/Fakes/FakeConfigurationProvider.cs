﻿using System.Diagnostics.CodeAnalysis;
using Services.Configuration.Contracts;

namespace Services.CSVImport.UnitTests.Reader.Fakes
{
    [ExcludeFromCodeCoverage]
    public class FakeConfigurationProvider : IConfigurationProvider
    {
        public string GetApplicationSettings(string settingName)
        {
            switch (settingName)
            {
                case "DataFolderName":
                    return IsValidData ? "TestData" : "TestData1";
                case "DataSeparator":
                    return IsValidSeparator ? ";" : "|";
                default:
                    return string.Empty;
            }
        }

        public bool IsValidData { get; set; }
        public bool IsValidSeparator { get; set; }

        public string GetConnectionStrings(string settingName)
        {
            throw new System.NotImplementedException();
        }
    }
}
