﻿using System.Collections.Generic;
using Services.CSVImport.Contracts;

namespace Services.CSVImport.UnitTests.Reader.Fakes
{
    public class FakeCSVImportProvider : ICSVImportProvider
    {
        public IEnumerable<string> ReadDataFileContent()
        {
            if (IsValidData)
            {
                return new List<string>
                {
                    "SKU;Product Name;Product Type;Quantity",
                    "F-M7ncpy;Fairytale 1;Book;23",
                    "F-4JshmZ;Album 2;CD;2",
                    "F-qdS7HZ;Novel 3;BOOK;14",
                    "F-S4d7PK;Poetry 4;book;0",
                    "F-8nQw53;Training Material 5;cd;5"
                };
            }

            if (HasNoHeader)
            {
                return new List<string>
                {
                    "F-M7ncpy;Fairytale 1;Book;23",
                    "F-4JshmZ;;CD;2",
                    "F-qdS7HZ;Novel 3;BOOK;14",
                    "F-S4d7PK;Poetry 4;book;0",
                    "F-8nQw53;Training Material 5;cd;5"
                };
            }

            if (HasNoContent)
            {
                return new List<string>();
            }

            return new List<string>
            {
                "SKU;Product Name;Product Type;Quantity",
                "F-M7ncpy;Fairytale 1;Book;23",
                "F-4JshmZ;;CD;2",
                "F-qdS7HZ;Novel 3;BOOK;14",
                "F-S4d7PK;Poetry 4;book;0",
                "F-8nQw53;Training Material 5;cd;5"
            };
        }

        public bool IsValidData { get; set; }

        public bool HasNoHeader { get; set; }

        public bool HasNoContent { get; set; }
    }
}
