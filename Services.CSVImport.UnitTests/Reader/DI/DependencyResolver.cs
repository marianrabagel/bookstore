﻿using System.Diagnostics.CodeAnalysis;
using Autofac;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using BookStore.Domain.Validators;
using FluentValidation;
using Services.Configuration;
using Services.Configuration.Contracts;
using Services.Configuration.DI.Contracts;
using Services.CSVImport.Contracts;
using Services.CSVImport.Contracts.Facades;
using Services.CSVImport.Facades;
using Services.CSVImport.UnitTests.Reader.Fakes;

namespace Services.CSVImport.UnitTests.Reader.DI
{
    [ExcludeFromCodeCoverage]
    public class DependencyResolver : AbstractDependencyResolver
    {
        public override void PerformRegistrations()
        {
            ContainerBuilder.RegisterType<FakeConfigurationProvider>().As<IConfigurationProvider>().SingleInstance();
            ContainerBuilder.RegisterType<ConfigurationReader>().As<IConfigurationReader>();

            ContainerBuilder.RegisterType<ProductsListValidator>().As<AbstractListValidator<ProductsList, Product>>();
            ContainerBuilder.RegisterType<ProductValidator>().As<AbstractValidator<Product>>();
            ContainerBuilder.RegisterType<ProductTypeValidator>().As<AbstractValidator<ProductType>>();
            ContainerBuilder.RegisterType<FakeCSVImportProvider>().As<ICSVImportProvider>().SingleInstance();
            ContainerBuilder.RegisterType<CSVImportReader>().As<ICSVImportReader>();

            ContainerBuilder.RegisterType<CSVImportReaderFacade>().As<ICSVImportReaderFacade>().PropertiesAutowired();

            base.PerformRegistrations();
        }
    }
}
