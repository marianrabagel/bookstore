﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Configuration.Contracts;
using Services.CSVImport.Contracts;
using Services.CSVImport.UnitTests.Reader.DI;
using Services.CSVImport.UnitTests.Reader.Fakes;
using Services.UnitTests;

namespace Services.CSVImport.UnitTests.Reader
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class CSVImportReaderTests : BaseTests<DependencyResolver>
    {
        private FakeConfigurationProvider configurationProvider;
        private ICSVImportReader importReader;
        private FakeCSVImportProvider importProvider;

        [TestInitialize]
        public void SetUp()
        {
            importProvider = (FakeCSVImportProvider)DependencyResolver.CreateInstance<ICSVImportProvider>();
            importReader = DependencyResolver.CreateInstance<ICSVImportReader>();

            configurationProvider = (FakeConfigurationProvider)DependencyResolver.CreateInstance<IConfigurationProvider>();
        }

        [TestMethod]
        public void TestThatTheCSVValidDataIsParsedCorrectly()
        {
            importProvider.IsValidData = true;
            importProvider.HasNoHeader = false;
            importProvider.HasNoContent = false;

            configurationProvider.IsValidData = true;
            configurationProvider.IsValidSeparator = true;

            var productsList = importReader.GetProductsListFromCSV();

            productsList.ValidateAndThrow();
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(ValidationException), @"Validation failed: 
 -- 'Product Name' should not be empty.
 -- 'Product Name' must be between 1 and 250 characters. You entered 0 characters.")]
        public void TestThatTheParsingOfCSVInvalidValidDataIsThrowingValidationException()
        {
            importProvider.IsValidData = false;
            importProvider.HasNoHeader = false;
            importProvider.HasNoContent = false;

            configurationProvider.IsValidData = true;
            configurationProvider.IsValidSeparator = true;

            var productsList = importReader.GetProductsListFromCSV();

            productsList.ValidateAndThrow();
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(InvalidOperationException), "Invalid CSV file")]
        public void TestThatAnInvalidOperationExceptionIsThrownWhenCSVInvalidSeparator()
        {
            importProvider.IsValidData = true;
            importProvider.HasNoHeader = false;
            importProvider.HasNoContent = false;

            configurationProvider.IsValidData = true;
            configurationProvider.IsValidSeparator = false;

            var productsList = importReader.GetProductsListFromCSV();

            productsList.ValidateAndThrow();
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(InvalidOperationException), "Invalid CSV file")]
        public void TestThatAnInvalidOperationExceptionIsThrownWhenCSVHasNoHeaderLine()
        {
            importProvider.IsValidData = false;
            importProvider.HasNoHeader = true;
            importProvider.HasNoContent = false;

            configurationProvider.IsValidData = false;
            configurationProvider.IsValidSeparator = false;

            var productsList = importReader.GetProductsListFromCSV();

            productsList.ValidateAndThrow();
        }

        [TestMethod]
        [Services.UnitTests.ExpectedException(typeof(ValidationException), @"Validation failed: 
 -- 'BookStore.Domain.Entities.ProductsList' should not be empty.")]
        public void TestThatAValidationExceptionIsThrownWhenCSVHasNoLines()
        {
            importProvider.IsValidData = false;
            importProvider.HasNoHeader = false;
            importProvider.HasNoContent = true;

            configurationProvider.IsValidData = false;
            configurationProvider.IsValidSeparator = false;

            var productsList = importReader.GetProductsListFromCSV();

            productsList.ValidateAndThrow();
        }
    }
}
