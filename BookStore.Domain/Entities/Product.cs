﻿using FluentValidation;
using System;
using BookStore.Domain.Contracts.Entities;

namespace BookStore.Domain.Entities
{
    [Serializable]
    public class Product : DomainEntity
    {
        private readonly AbstractValidator<Product> productValidator;

        public Product(AbstractValidator<Product> productValidator, AbstractValidator<ProductType> productTypeValidator)
        {
            this.productValidator = productValidator;

            Type = new ProductType(productTypeValidator);
        }

        public int ProductId { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
        public ProductType Type { get; set; }
        public int Quantity { get; set; }

        public override void ValidateAndThrow()
        {
            productValidator.ValidateAndThrow(this);
        }
    }
}