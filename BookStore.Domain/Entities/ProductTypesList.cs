﻿using BookStore.Domain.Contracts.Entities;
using BookStore.Domain.Contracts.Validators;
using FluentValidation;

namespace BookStore.Domain.Entities
{
    public class ProductTypesList : DomainEntitiesList<ProductType>
    {
        private readonly AbstractListValidator<ProductTypesList, ProductType> productTypesListValidator;

        public ProductTypesList(AbstractListValidator<ProductTypesList, ProductType> productTypesListValidator)
        {
            this.productTypesListValidator = productTypesListValidator;
        }

        public override void ValidateAndThrow()
        {
            productTypesListValidator.ValidateAndThrow(this);
        }
    }
}
