﻿using FluentValidation;
using System;
using BookStore.Domain.Contracts.Entities;

namespace BookStore.Domain.Entities
{
    [Serializable]
    public class ProductType : DomainEntity
    {
        private readonly AbstractValidator<ProductType> productTypeValidator;

        public ProductType(AbstractValidator<ProductType> productTypeValidator)
        {
            this.productTypeValidator = productTypeValidator;
        }

        public int ProductTypeId { get; set; }
        public string Name { get; set; }

        public override void ValidateAndThrow()
        {
            productTypeValidator.ValidateAndThrow(this);
        }
    }
}
