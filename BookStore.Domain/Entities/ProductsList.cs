﻿using BookStore.Domain.Contracts.Entities;
using BookStore.Domain.Contracts.Validators;
using FluentValidation;
using Newtonsoft.Json;

namespace BookStore.Domain.Entities
{
    public class ProductsList : DomainEntitiesList<Product>
    {
        private readonly AbstractListValidator<ProductsList, Product> productsListValidator;

        public ProductsList(AbstractListValidator<ProductsList, Product> productsListValidator)
        {
            this.productsListValidator = productsListValidator;
        }

        public override void ValidateAndThrow()
        {
            productsListValidator.ValidateAndThrow(this);
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
