﻿using BookStore.Domain.Entities;
using FluentValidation;

namespace BookStore.Domain.Validators
{
    public class ProductTypeValidator : AbstractValidator<ProductType>
    {
        public ProductTypeValidator()
        {
            RuleFor(prod => prod.Name)
                .NotEmpty()
                .Length(1, 50);
        }
    }
}
