﻿using System;
using System.Diagnostics.CodeAnalysis;
using BookStore.Domain.Contracts.Validators;
using BookStore.Domain.Entities;
using FluentValidation;

namespace BookStore.Domain.Validators
{
    [ExcludeFromCodeCoverage]
    [Serializable]
    public class ProductTypesListValidator : AbstractListValidator<ProductTypesList, ProductType>
    {
        public ProductTypesListValidator()
        {
            RuleFor(list => list)
                .NotEmpty();
        }
    }
}
