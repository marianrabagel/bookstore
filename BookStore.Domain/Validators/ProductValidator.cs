﻿using BookStore.Domain.Entities;
using FluentValidation;

namespace BookStore.Domain.Validators
{
    public class ProductValidator : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(prod => prod.SKU)
                .NotEmpty()
                .Length(1, 10);

            RuleFor(prod => prod.Name)
                .NotEmpty()
                .Length(1, 250);

            RuleFor(prod => prod.Type)
                .NotEmpty()
                .SetValidator(new ProductTypeValidator());

            RuleFor(prod => prod.Quantity)
                .GreaterThanOrEqualTo(0);
        }
    }
}
