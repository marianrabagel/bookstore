﻿namespace BookStore.Domain.Contracts.Entities
{
    public abstract class DomainEntity
    {
        public abstract void ValidateAndThrow();
    }
}