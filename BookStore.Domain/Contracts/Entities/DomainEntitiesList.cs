﻿using System;
using System.Collections;
using System.Collections.Generic;
using BookStore.Domain.Contracts.Validators;

namespace BookStore.Domain.Contracts.Entities
{
    public abstract class DomainEntitiesList<T> : IList<T>, IValidateable
        where T : DomainEntity
    {
        private const string ErrMsgOperationIsNotPermittedOnAReadonlyList = "This operation is not permitted on a ReadOnly list!";

        private readonly IList<T> domainEntities;
        private bool isReadOnly;

        protected DomainEntitiesList()
        {
            domainEntities = new List<T>();
        }

        public int IndexOf(T item)
        {
            return domainEntities.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            CheckIfListIsReadonly();
            domainEntities.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            CheckIfListIsReadonly();
            domainEntities.RemoveAt(index);
        }

        public T this[int index]
        {
            get { return domainEntities[index]; }
            set
            {
                CheckIfListIsReadonly();
                domainEntities[index] = value;
            }
        }

        public void Add(T item)
        {
            CheckIfListIsReadonly();
            domainEntities.Add(item);
        }

        public void AddRange(IEnumerable<T> collection)
        {
            CheckIfListIsReadonly();
            foreach (var item in collection)
            {
                Add(item);
            }
        }

        public void Clear()
        {
            CheckIfListIsReadonly();
            domainEntities.Clear();
        }

        public bool Contains(T item)
        {
            return domainEntities.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            domainEntities.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return domainEntities.Count; }
        }

        public bool IsReadOnly
        {
            get { return isReadOnly; }
            protected set { isReadOnly = value; }
        }

        public bool Remove(T item)
        {
            CheckIfListIsReadonly();
            return domainEntities.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return domainEntities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public abstract void ValidateAndThrow();

        private void CheckIfListIsReadonly()
        {
            if (isReadOnly)
            {
                throw new InvalidOperationException(ErrMsgOperationIsNotPermittedOnAReadonlyList);
            }
        }
    }
}