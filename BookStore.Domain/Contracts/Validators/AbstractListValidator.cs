﻿using System.Collections.Generic;
using BookStore.Domain.Contracts.Entities;
using FluentValidation;
using FluentValidation.Results;

namespace BookStore.Domain.Contracts.Validators
{
    public abstract class AbstractListValidator<TList, TListItem> : AbstractValidator<TList>
        where TList : DomainEntitiesList<TListItem>
        where TListItem : DomainEntity
    {
        protected AbstractListValidator()
        {
            ValidationMessageFormatter.ChangeDefaultBehaviour();
        }

        public override ValidationResult Validate(ValidationContext<TList> context)
        {
            var validationResult = base.Validate(context);

            var validationFailuresList = new List<ValidationFailure>();

            validationFailuresList.AddRange(ValidationMessageFormatter.AddValidationFailuresToList(validationResult.Errors));

            foreach (var listItem in context.InstanceToValidate)
            {
                try
                {
                    listItem.ValidateAndThrow();
                }
                catch (ValidationException validationException)
                {
                    validationFailuresList.AddRange(ValidationMessageFormatter.AddValidationFailuresToList(validationException.Errors));
                }
            }

            return new ValidationResult(validationFailuresList);
        }
    }

}
