﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;
using FluentValidation;
using FluentValidation.Results;

namespace BookStore.Domain.Contracts.Validators
{
    internal static class ValidationMessageFormatter
    {
        public static void ChangeDefaultBehaviour()
        {
            ValidatorOptions.DisplayNameResolver = (type, member, arg3) => member != null ?
                string.Format(CultureInfo.InvariantCulture, "{0} {1}", type.Name, member.Name) :
                null;
        }

        public static List<ValidationFailure> AddValidationFailuresToList(IEnumerable<ValidationFailure> validationFailures)
        {
            var validationFailuresList = new List<ValidationFailure>();

            foreach (var error in validationFailures)
            {
                string validationFailureMessage;

                if (error.ErrorMessage.Contains("{") && error.ErrorMessage.Contains("}"))
                {
                    validationFailureMessage = error.ErrorMessage;
                }
                else
                {
                    if (error.FormattedMessagePlaceholderValues != null)
                    {
                        var message = error.ErrorMessage.Replace("''", "'{0}'");
                        var valuesStringBuilder = new StringBuilder();

                        foreach (var formattedMessagePlaceholderValue in error.FormattedMessagePlaceholderValues)
                        {
                            if (formattedMessagePlaceholderValue.Value != null)
                            {
                                valuesStringBuilder.Append(formattedMessagePlaceholderValue.Value);
                            }
                        }

                        validationFailureMessage = string.Format(message, valuesStringBuilder);
                    }
                    else
                    {
                        validationFailureMessage = error.ErrorMessage;
                    }
                }

                var validationFailure = new ValidationFailure(error.PropertyName, validationFailureMessage);
                validationFailuresList.Add(validationFailure);
            }

            return validationFailuresList;
        }
    }
}
