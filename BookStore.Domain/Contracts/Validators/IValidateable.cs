﻿namespace BookStore.Domain.Contracts.Validators
{
    public interface IValidateable
    {
        void ValidateAndThrow();
    }
}
